#pragma once
#include <string>
#include <type_traits>
#include <string_view>

namespace Hmv
{
	using string_type = std::string;
	using string_view_type = std::string_view;

	template <typename Artifact>
		requires (std::is_default_constructible_v<Artifact>)
	struct ParseResult
	{
		string_view_type remaining; ///< The remaining characters in the string.
		Artifact artifact;          ///< The data that has been parsed from the string.
		/**
		 * @brief Check if the parse result is valid.
		 * 
		 * @return true 
		 * @return false 
		 */
		[[nodiscard]] constexpr bool IsValid() const noexcept
		{
			return this->remaining.data() != nullptr;
		}
		friend bool operator==(ParseResult const & lhs, ParseResult const& rhs) = default;
	};
	/**
	 * @brief Get the length of some parsed result.
	 * 
	 * @tparam Artifact The type of the artifact.
	 * @param before The string before some operation took place.
	 * @param parse_result The result of a parse operation.
	 * @return The length of the section that was parsed.
	 */
	template <typename Artifact>
	[[nodiscard]] constexpr auto GetParsedLength(string_view_type before, ParseResult<Artifact> const & parse_result) noexcept -> std::size_t
	{
		if (not parse_result.IsValid()) return std::size_t(0);
		return static_cast<std::size_t>(parse_result.remaining.data() - before.data());
	}
}