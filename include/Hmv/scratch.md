# From
```mermaid
flowchart TD
	classDef Red fill:#f00
	classDef Black fill:#000,color:#fff
	classDef Null display: none;
	root:12:::Black
	8:::Red
	15:::Black
	1:::Black
	23:::Red
	10:::Black
	9:::Red
	13:::Red
	root:12 -->|left| 8
	root:12 -->|right| 15
	8 -->|left| 1
	8 -->|right| 10
	15 -->|left| 13
	15 -->|right| 23
	1 ~~~ 0null:::Null
	1 ~~~ 1null:::Null
	23 ~~~ 2null:::Null
	23 ~~~ 3null:::Null
	10 -->|left| 9
	10 ~~~ 4null:::Null
	9 ~~~ 5null:::Null
	9 ~~~ 6null:::Null
	13 ~~~ 7null:::Null
	13 ~~~ 8null:::Null
	subgraph Recycling
	end


```
# Removed 12 
```mermaid
flowchart TD
	classDef Red fill:#f00
	classDef Black fill:#000,color:#fff
	classDef Null display: none;
	12:::Black
	8:::Red
	15:::Black
	1:::Black
	23:::Red
	10:::Black
	9:::Red
	root:13:::Black
	12 ~~~ 0null:::Null
	12 ~~~ 1null:::Null
	8 -->|left| 1
	8 -->|right| 10
	15 ~~~ 2null:::Null
	15 -->|right| 23
	1 ~~~ 3null:::Null
	1 ~~~ 4null:::Null
	23 ~~~ 5null:::Null
	23 ~~~ 6null:::Null
	10 -->|left| 9
	10 ~~~ 7null:::Null
	9 ~~~ 8null:::Null
	9 ~~~ 9null:::Null
	root:13 -->|left| 8
	root:13 -->|right| 15
	subgraph Recycling
		12
	end


```
# Removed 1 
```mermaid
flowchart TD
	classDef Red fill:#f00
	classDef Black fill:#000,color:#fff
	classDef Null display: none;
	root:12:::Black
	8:::Black
	15:::Black
	1:::Black
	23:::Red
	10:::Black
	9:::Red
	13:::Red
	root:12 -->|left| 8
	root:12 -->|right| 15
	8 ~~~ 0null:::Null
	8 -->|right| 10
	15 -->|left| 13
	15 -->|right| 23
	1 ~~~ 1null:::Null
	1 ~~~ 2null:::Null
	23 ~~~ 3null:::Null
	23 ~~~ 4null:::Null
	10 -->|left| 9
	10 ~~~ 5null:::Null
	9 ~~~ 6null:::Null
	9 ~~~ 7null:::Null
	13 ~~~ 8null:::Null
	13 ~~~ 9null:::Null
	subgraph Recycling
		1
	end


```
# Removed 10 
```mermaid
flowchart TD
	classDef Red fill:#f00
	classDef Black fill:#000,color:#fff
	classDef Null display: none;
	root:12:::Black
	8:::Red
	15:::Black
	1:::Black
	23:::Red
	10:::Black
	9:::Black
	13:::Red
	root:12 -->|left| 8
	root:12 -->|right| 15
	8 -->|left| 1
	8 -->|right| 9
	15 -->|left| 13
	15 -->|right| 23
	1 ~~~ 0null:::Null
	1 ~~~ 1null:::Null
	23 ~~~ 2null:::Null
	23 ~~~ 3null:::Null
	10 ~~~ 4null:::Null
	10 ~~~ 5null:::Null
	9 ~~~ 6null:::Null
	9 ~~~ 7null:::Null
	13 ~~~ 8null:::Null
	13 ~~~ 9null:::Null
	subgraph Recycling
		10
	end


```
# Removed 8 
```mermaid
flowchart TD
	classDef Red fill:#f00
	classDef Black fill:#000,color:#fff
	classDef Null display: none;
	root:12:::Black
	8:::Red
	15:::Black
	1:::Black
	23:::Red
	10:::Black
	9:::Red
	13:::Red
	root:12 -->|left| 9
	root:12 -->|right| 15
	8 ~~~ 0null:::Null
	8 ~~~ 1null:::Null
	15 -->|left| 13
	15 -->|right| 23
	1 ~~~ 2null:::Null
	1 ~~~ 3null:::Null
	23 ~~~ 4null:::Null
	23 ~~~ 5null:::Null
	10 ~~~ 6null:::Null
	10 ~~~ 7null:::Null
	9 -->|left| 1
	9 -->|right| 10
	13 ~~~ 8null:::Null
	13 ~~~ 9null:::Null
	subgraph Recycling
		8
	end


```
# Removed 9 
```mermaid
flowchart TD
	classDef Red fill:#f00
	classDef Black fill:#000,color:#fff
	classDef Null display: none;
	root:12:::Black
	8:::Red
	15:::Black
	1:::Black
	23:::Red
	10:::Black
	9:::Red
	13:::Red
	root:12 -->|left| 8
	root:12 -->|right| 15
	8 -->|left| 1
	8 -->|right| 10
	15 -->|left| 13
	15 -->|right| 23
	1 ~~~ 0null:::Null
	1 ~~~ 1null:::Null
	23 ~~~ 2null:::Null
	23 ~~~ 3null:::Null
	10 ~~~ 4null:::Null
	10 ~~~ 5null:::Null
	9 ~~~ 6null:::Null
	9 ~~~ 7null:::Null
	13 ~~~ 8null:::Null
	13 ~~~ 9null:::Null
	subgraph Recycling
		9
	end


```
# Removed 15 
```mermaid
flowchart TD
	classDef Red fill:#f00
	classDef Black fill:#000,color:#fff
	classDef Null display: none;
	root:12:::Black
	8:::Red
	15:::Black
	1:::Black
	23:::Black
	10:::Black
	9:::Red
	13:::Red
	root:12 -->|left| 8
	root:12 -->|right| 23
	8 -->|left| 1
	8 -->|right| 10
	15 ~~~ 0null:::Null
	15 ~~~ 1null:::Null
	1 ~~~ 2null:::Null
	1 ~~~ 3null:::Null
	23 -->|left| 13
	23 ~~~ 4null:::Null
	10 -->|left| 9
	10 ~~~ 5null:::Null
	9 ~~~ 6null:::Null
	9 ~~~ 7null:::Null
	13 ~~~ 8null:::Null
	13 ~~~ 9null:::Null
	subgraph Recycling
		15
	end


```
# Removed 13 
```mermaid
flowchart TD
	classDef Red fill:#f00
	classDef Black fill:#000,color:#fff
	classDef Null display: none;
	root:12:::Black
	8:::Red
	15:::Black
	1:::Black
	23:::Red
	10:::Black
	9:::Red
	13:::Red
	root:12 -->|left| 8
	root:12 -->|right| 15
	8 -->|left| 1
	8 -->|right| 10
	15 ~~~ 0null:::Null
	15 -->|right| 23
	1 ~~~ 1null:::Null
	1 ~~~ 2null:::Null
	23 ~~~ 3null:::Null
	23 ~~~ 4null:::Null
	10 -->|left| 9
	10 ~~~ 5null:::Null
	9 ~~~ 6null:::Null
	9 ~~~ 7null:::Null
	13 ~~~ 8null:::Null
	13 ~~~ 9null:::Null
	subgraph Recycling
		13
	end


```
# Removed 23 
```mermaid
flowchart TD
	classDef Red fill:#f00
	classDef Black fill:#000,color:#fff
	classDef Null display: none;
	root:12:::Black
	8:::Red
	15:::Black
	1:::Black
	23:::Red
	10:::Black
	9:::Red
	13:::Red
	root:12 -->|left| 8
	root:12 -->|right| 15
	8 -->|left| 1
	8 -->|right| 10
	15 -->|left| 13
	15 ~~~ 0null:::Null
	1 ~~~ 1null:::Null
	1 ~~~ 2null:::Null
	23 ~~~ 3null:::Null
	23 ~~~ 4null:::Null
	10 -->|left| 9
	10 ~~~ 5null:::Null
	9 ~~~ 6null:::Null
	9 ~~~ 7null:::Null
	13 ~~~ 8null:::Null
	13 ~~~ 9null:::Null
	subgraph Recycling
		23
	end


```
