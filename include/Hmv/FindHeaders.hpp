#pragma once
#include "Hmv/ParseCommon.hpp"
#include <vector>

namespace Hmv
{
	/**
	 * @brief The search options for C/C++ headers.
	 */
	struct HeaderSearchOptions
	{
		bool include_quoted = true; ///< Whether or not to scan for quoted headers `#include "hello.hpp"`
		bool include_angle_brackets = true; ///< Whether or not to scan for angle bracket headers '#include <vector>'
		/**
		 * @brief Check if a character is a viable open character for a header.
		 *
		 * @param input The character to check.
		 * @return The character is a viable open character.
		 */
		 [[nodiscard]] auto IsOpenChar(string_view_type::value_type input) const noexcept;
		 /**
		  * @brief Get the matching close character.
		  * 
		  * @param open_char The close character to check.
		  * @return The close character.
		  */
		 [[nodiscard]] auto GetCloseChar(string_view_type::value_type open_char) const noexcept;
		 /**
		  * @brief Check if a particular character can belong in a particular header type.
		  * 
		  * @param open_char The character to check.
		  * @param input The character to check.
		  * @return true when the character can belong in the open_char type.
		  */
		[[nodiscard]] auto IsAllowedChar(string_view_type::value_type open_char, string_view_type::value_type input) const noexcept;
	};
	/**
	 * @brief A parse result containing the header name found.
	 */
	using ParseIncludeResult = ParseResult<string_view_type>;
	/**
	 * @brief Parse some include, providing the header name if parse is successful.
	 * 
	 * This is the string after the '#include', for example:
	 * @li `<vector>`
	 * @li `"Header.hpp"`
	 * 
	 * @param input The string to parse.
	 * @param options The parsing options for the include.
	 * @return The parse result with header name as the `artifact`.
	 */
	[[nodiscard]] auto ParseIncludeString(string_view_type input, HeaderSearchOptions const & options = {}) -> ParseIncludeResult;
	/**
	 * @brief A parse result containing the header name found.
	 */
	using ParseIncludeResult = ParseResult<string_view_type>;
	/**
	 * @brief Parse some include, providing the header name if parse is successful.
	 * 
	 * @param input The string to parse.
	 * @param options The parsing options for the include.
	 * @return The parse result with header name as the `artifact`.
	 */
	[[nodiscard]] auto ParseInclude(string_view_type input, HeaderSearchOptions const & options = {}) -> ParseIncludeResult;
	/**
	 * @brief Find the first header name in some input string.
	 * 
	 * @param input The string to parse.
	 * @param options The parsing options for the include.
	 * @return The parse result with the header name found as the `artifact`.
	 */
	[[nodiscard]] auto FindFirstInclude(string_view_type input, HeaderSearchOptions const & options = {}) -> ParseIncludeResult;
	/**
	 * @brief A list of headers.
	 */
	using HeaderList = std::vector<string_view_type>;
	/**
	 * @brief Find the headers included in some source file.
	 * 
	 * @param contents The contents of the file to scan.
	 * @param options The parsing options for the include.
	 * @return A list of headers.
	 */
	[[nodiscard]] auto FindAllIncludes(string_view_type contents, HeaderSearchOptions const & options = {}) -> HeaderList;
}