#pragma once
#include "Hmv/Range.hpp"
#include <string>
#include <cstddef>

namespace Hmv
{
	struct SourceLineColumn
	{
		std::size_t line = 0;
		std::size_t column = 0;
	};

	struct SourceLocation
	{
		std::u8string filename;
		IndexRange span;
	};
}