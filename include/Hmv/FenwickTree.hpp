#pragma once
#include "Hmv/FenwickTree.hpp"
#include <climits>
#include <concepts>
#include <functional>
#include <initializer_list>
#include <ranges>
#include <type_traits>
#include <vector>
#include <span>
#include <bit>

namespace Hmv
{
	template <std::integral IndexType, auto Op = std::plus<IndexType>{}>
	struct FenwickTree
	{
		using index_type = IndexType;

		struct SumAndLength
		{
			index_type sum;
			index_type length;
		};

		[[nodiscard]] constexpr auto QuerySum(index_type i) const noexcept -> index_type
		{
			index_type sum = m_tree[0];
			while (i != 0)
			{
				sum += m_tree[i];
				i -= LowestOneBit(i);
			}
			return sum;
		}

		[[nodiscard]] constexpr auto QueryLength(index_type i) const noexcept -> index_type
		{
			return QuerySum(i+1) - QuerySum(i);
		}

		[[nodiscard]] constexpr auto QuerySumAndLength(index_type i) const noexcept -> SumAndLength
		{
			auto sum = QuerySum(i);
			auto next_sum = QuerySum(i + 1);
			return SumAndLength{
				.sum = sum,
				.length = next_sum - sum
			};
		}

		[[nodiscard]] constexpr auto operator[](index_type i) const noexcept -> SumAndLength
		{
			return QuerySumAndLength(i);
		}

		[[nodiscard]] constexpr auto Size() const noexcept -> index_type
		{
			return static_cast<index_type>(m_tree.m_tree.size() + 1);
		}

		constexpr void Update(index_type i, index_type delta) noexcept
		{
			++i;
			while (i < std::size(m_tree))
			{
				m_tree[i] += delta;
				i += LowestOneBit(i);
			}
		}

		[[nodiscard]] constexpr index_type PushBack(index_type value) noexcept
		{
			auto i = m_tree.size();
			m_tree.emplace_back(value);
			auto sum = i - (LowestOneBit(i) >> 1);
			if (sum != i) { m_tree.back() += m_tree[sum]; }
			return i - 1;
		}

		constexpr void SetLength(index_type i, index_type value) noexcept
		{
			auto const old_length = QueryLength(i);
			auto const delta_length = value - old_length;
			Update(i, delta_length);
		}

		constexpr FenwickTree(std::span<index_type const> values)
		{
			m_tree.reserve(values.size() + 1);
			m_tree.push_back({ 0 });
			m_tree.insert(m_tree.end(), values.begin(), values.end());
			auto const n = m_tree.size();
			for (index_type i = 1; i < n; ++i)
			{
				auto const p = i + LowestOneBit(i);
				if (p < n) { m_tree[p] += m_tree[i]; }
			}
		}

		constexpr FenwickTree(std::initializer_list<index_type> values)
			: FenwickTree{ std::span<index_type const>{ values.begin(), values.size() } }
		{}
	private:
		struct value_type_wrapper
		{
			index_type value = {}; ///< Ensure always initialized.
			/**
			 * @brief Avoid breaking other operations.
			 * 
			 * @return The value stored in this object.
			 */
			[[nodiscard]] constexpr operator index_type() const noexcept
			{
				return { value };
			}
			/**
			 * @brief Add operator support, without infecting the code.
			 * 
			 * @param elem The elemnet to add.
			 * @return The current object.
			 */
			constexpr value_type_wrapper & operator+=(index_type elem) noexcept
			{
				value = Op(value, elem);
				return *this;
			}

			constexpr value_type_wrapper(index_type v)
				: value(v)
			{}
		};
		[[nodiscard]] static constexpr auto LowestOneBit(index_type value) noexcept -> index_type
		{
			return value & -value;
		}
		static constexpr index_type const bit_count = sizeof(index_type) * CHAR_BIT;
		static constexpr index_type const all_ones = ~index_type{0};
		[[nodiscard]] static constexpr auto TrailingZerosBitMask(index_type value) noexcept -> index_type
		{
			if (value == 0) return 0;
			// This is completely UB if value is zero...
			return (static_cast<index_type>(1) << std::countr_zero<index_type>(value)) - 1;
		}
		static_assert(LowestOneBit(0b0001) == index_type{0b0001});
		static_assert(LowestOneBit(0b0011) == index_type{0b0001});
		static_assert(LowestOneBit(0b0010) == index_type{0b0010});
		static_assert(LowestOneBit(0b0110) == index_type{0b0010});

		std::vector<value_type_wrapper> m_tree;
	};
}