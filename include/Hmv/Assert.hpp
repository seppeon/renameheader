#pragma once
#include <type_traits>
#include <cassert>
#include <exception>

/**
 * @brief Could be alot better, but this allows it in constexpr.
 */
#define HMV_ASSERT(x) do{ if (std::is_constant_evaluated()) { if (not static_cast<bool>(x)) { std::terminate(); } } else { assert((x)); } } while(false);