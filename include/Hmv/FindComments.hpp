#pragma once
#include "Hmv/ParseCommon.hpp"

namespace Hmv
{
	/**
	 * @brief Strip comments from C/C++ source.
	 * 
	 * @param input The source text.
	 * @return Text with no comments.
	 */
	[[nodiscard]] auto StripComments(string_view_type input) -> string_type;
}