#pragma once
#include "Hmv/ParseCommon.hpp"
#include "Hmv/FenwickTree.hpp"
#include "Hmv/Range.hpp"
#include <stack>

namespace Hmv
{
	struct SourceFile;

	using SourceRange = IndexRange;
	/**
	 * @brief Represent some file, with changes applied on top.
	 * 
	 * The content of the source file is always tracable to the section
	 * of original source it is relevant to.
	 */
	struct SourceFile
	{
		/**
		 * @brief Replace some section of the source file.
		 * 
		 * @param old_range The old range (indexes are after applying previous edits).
		 * @param new_data The data that will replace the range of data specified by `old_range`.
		 */
		void Replace(SourceRange old_range, string_view_type new_data);
		/**
		 * @brief Delete a section of the source file.
		 * 
		 * @param range The range in the source file (after applying previous edits) that will be deleted.
		 */
		void Delete(SourceRange range);
		/**
		 * @brief Apply an insertion to the source file.
		 * 
		 * @param index The index in the source file (after applying previous edits) that the data will be inserted.
		 * @param new_data The new data to insert.
		 */
		void Insert(std::size_t index, string_view_type new_data);
		/**
		 * @brief Get the number of characters in the source file (after applying edits).
		 * 
		 * @return The number of characters.
		 */
		[[nodiscard]] auto Size() const noexcept -> std::size_t;
		/**
		 * @brief Get the character at the particular index, after applying edits.
		 *
		 * @note There is no, non-const overload of this function, such a thing would break tracability of changes. 
		 *
		 * @param index The index.
		 * @return The character at that index.
		 */
		[[nodiscard]] auto operator[](std::size_t index) const noexcept -> string_type::const_reference;
		/**
		 * @brief A random access iterator into the source of the file.
		 */
		struct iterator{};
		/**
		 * @brief The begin iterator.
		 * 
		 * @return iterator.
		 */
		[[nodiscard]] auto begin() const noexcept -> iterator;
		/**
		 * @brief The end iterator.
		 * 
		 * @return iterator.
		 */
		[[nodiscard]] auto end() const noexcept -> iterator;
		/**
		 * @brief Map an index in the current source file to the position in the original file which it was derived.
		 *
		 * This is useful when source modification is required as part of parsing some language.
		 * For example, the C preprocessor needs to concatenate strings, and process preprocessor
		 * statments, such as `#ifdef`.
		 *
		 * Note, any source location can be represented by a range, since characters can be deleted.
		 *
		 * @return The range from the original source.
		 */
		[[nodiscard]] auto GetSourceLocation(std::size_t index) const noexcept -> SourceRange;
		/**
		 * @brief Map a range in the current source file to a range in the original file for which the current range was derived.
		 * 
		 * @param current_range The range in the source file.
		 * @return The range from the original source.
		 */
		[[nodiscard]] auto GetSourceLocation(SourceRange current_range) const noexcept -> SourceRange;
	private:
		string_type m_contents;
	};
}