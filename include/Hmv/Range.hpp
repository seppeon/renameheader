#pragma once
#include <cstddef>

namespace Hmv
{
	template <typename T>
	struct Range
	{
		T start = {};
		T length = {};
	};
	using IndexRange = Range<std::size_t>;
}