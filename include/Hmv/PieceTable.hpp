#pragma once
#include "Hmv/FenwickTree.hpp"
#include "Hmv/Range.hpp"
#include "Hmv/ParseCommon.hpp"
#include <vector>
#include <array>

namespace Hmv
{
	/**
	 * @brief Represent an index range within the piece table's internals.
	 * 
	 * @note This is for internal use only.
	 */
	struct PieceTableNode
	{
		std::size_t start;
		std::size_t length;
	};
	/**
	 * @brief Represent a string through use of a piece table.
	 */
	struct PieceTable
	{
		using container_type = string_type;
		using view_type = string_view_type;
		using size_type = typename container_type::size_type;
		using value_type = typename container_type::value_type;
		using reference = typename container_type::reference;
		using const_reference = typename container_type::const_reference;
		using pointer = typename container_type::pointer;
		using const_pointer = typename container_type::const_pointer;
		/**
		 * @brief Construct a piece table from some data.
		 * 
		 * @param data 
		 */
		PieceTable(container_type data);
		/**
		 * @brief Get a view of the original string.
		 * 
		 * @return A view of the original string.
		 */
		[[nodiscard]] auto OriginalContent() const noexcept -> string_view_type;
		/**
		 * @brief Get the number of characters accessible through `.operator[]`.
		 * 
		 * @return The number of characters.
		 */
		[[nodiscard]] auto Size() const noexcept -> size_type;
		/**
		 * @brief Get the item at a speciifc index.
		 * 
		 * @param index The index to retrieve.
		 * @return A constant reference to the element at the requested index. 
		 */
		[[nodiscard]] auto operator[](size_type index) const noexcept -> string_type::const_reference;
		/**
		 * @brief Append a string to the end.
		 * 
		 * @param new_data The data to append to this table.
		 */
		void Append(string_view_type new_data);
		/**
		 * @brief Replace some section of the piece table.
		 * 
		 * @param old_range The old range (indexes are after applying previous edits).
		 * @param new_data The data that will replace the range of data specified by `old_range`.
		 */
		void Replace(PieceTableNode old_range, string_view_type new_data);
		/**
		 * @brief Delete a section of the piece table.
		 * 
		 * @param range The range in the piece table (after applying previous edits) that will be deleted.
		 */
		void Delete(PieceTableNode range);
		/**
		 * @brief Apply an insertion to the piece table.
		 * 
		 * @param index The index in the piece table (after applying previous edits) that the data will be inserted.
		 * @param new_data The new data to insert.
		 */
		void Insert(size_type index, string_view_type new_data);
		/**
		 * @brief Renders the data represented in this table to a contigious string.
		 * 
		 * @return The contigious string.
		 */
		[[nodiscard]] auto ToString() const noexcept -> string_type;
	private:
		/**
		 * @brief Get the sum of the nodes.
		 * 
		 * @note This should always equal `.Size()` except where an exception was thrown during an operation.
		 * 
		 * @return The number of characters.
		 */
		[[nodiscard]] auto SumOfLengths() const noexcept -> size_type;

		size_type m_source_length = 0;
		size_type m_total_length = 0;
		container_type m_data;
		std::vector<PieceTableNode> m_nodes;
	};
}