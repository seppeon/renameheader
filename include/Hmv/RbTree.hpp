#pragma once
#include "Hmv/Assert.hpp"
#include <cstdint>
#include <iterator>
#include <memory>
#include <vector>
#include <string>
#include <concepts>

namespace Hmv
{
	using rb_node_id = std::uint32_t;
	inline constexpr rb_node_id rb_npos = (~rb_node_id{});

	enum class RbNodeSide : bool
	{
		Right = false,
		Left = true,
	};

	enum class RbColour : bool
	{
		Red = false,
		Black = true
	};

	[[nodiscard]] constexpr auto GetOtherSide(RbNodeSide side) -> RbNodeSide
	{
		return static_cast<RbNodeSide>(not static_cast<bool>(side));
	}

	template <typename K>
	concept RbTreeKey = requires(K key)
	{
		{ key < key } -> std::same_as<bool>;
	};

	template <RbTreeKey K, typename T>
	struct RbTreeValue
	{
		K key;
		T value;

		template <RbTreeKey K2, typename T2>
			requires(requires(K k, T t)
			{
				{ k } -> std::convertible_to<K2>;
				{ t } -> std::convertible_to<T2>;
			})
		constexpr operator RbTreeValue<K2, T2> () const noexcept
		{
			return RbTreeValue<K2, T2>{
				.key = key,
				.value = value,
			};
		}
	};
	/**
	 * @brief A densely stored red-black tree, usable as a set or map.
	 * 
	 * The rules of red black tree:
	 * @li Nodes are red or black.
	 * @li Root node is black.
	 * @li Empty nodes are black.
	 * @li Red nodes have black children.
	 * @li All paths to the leaves from the root have the same number of black nodes.
	 * ed
	 * @warning Your type must have a sane ordering, this will assume that items that are not less than, and are not greater than are equal.
	 * 
	 * @tparam K The key type.
	 * @tparam T The value type (or void).
	 * @tparam Alloc The allocator type.
	 */
	template <RbTreeKey K, typename T = void, template <typename> class Alloc = std::allocator>
	struct RbTree
	{
		using node_id = rb_node_id;

		[[nodiscard]] constexpr bool Insert(K const & key, T const & value)
		{
			// The first insertion is easiest.
			if (m_root == rb_npos)
			{
				SetRoot(NewNode(key, value, RbColour::Black));
				return true;
			}

			// Traverse the tree for an insertion.
			auto const [found_side, found_node, _, parent_node] = FindNode(key, m_root);

			// Cannot insert if already exists.
			if (found_node != rb_npos) return false;

			// Cannot insert if parent doesn't exist.
			if (parent_node == rb_npos) return false;

			// Cursor is now at the insertion point, and insert side the preferred side.
			auto const new_node = NewNode(key, value);
			Link(parent_node, new_node, found_side);

			// We now have a binary tree, lets adjust the darn RB tree.
			FixAfterInsert(new_node);
			return true;
		}

		[[nodiscard]] constexpr bool Remove(K const & key)
		{
			auto const [found_side, found_node, parent_side, parent_node] = FindNode(key, m_root);
			// The node that was attempted to remove, didn't exist.
			if (found_node == rb_npos) return false;

			auto const node_colour = GetColour(found_node);
			auto const left_child = GetLeftChild(found_node);
			auto const right_child = GetRightChild(found_node);
			if (IsNull(left_child) and IsNull(right_child))
			{
				ClearChild(parent_node, found_side);
				if (node_colour == RbColour::Black)
				{
					FixAfterRemove(parent_node);
				}
			}
			else if (IsNull(left_child) and IsValid(right_child))
			{
				Link(parent_node, right_child, found_side);
				if (node_colour == RbColour::Black)
				{
					FixAfterRemove(right_child);
				}
			}
			else if (IsValid(left_child) and IsNull(right_child))
			{
				Link(parent_node, left_child, found_side);
				if (node_colour == RbColour::Black)
				{
					FixAfterRemove(left_child);
				}
			}
			else
			{
				auto const [_, right_found_node, replacement_node_side, replacement_node] = FindNode(key, right_child);
				assert(right_found_node == rb_npos);
				auto const replacement_colour = GetColour(replacement_node);
				auto const replacement_node_parent = GetParent(replacement_node);
				Link(parent_node, replacement_node, found_side);
				Link(replacement_node, left_child, RbNodeSide::Left);
				if (replacement_node_parent != found_node)
				{
					ClearChild(replacement_node_parent, replacement_node_side);
					Link(replacement_node, right_child, RbNodeSide::Right);
				}
				SetColour(replacement_node, node_colour);
				if (replacement_colour == RbColour::Black)
				{
					FixAfterRemove(replacement_node_parent, replacement_node, replacement_node_side);
				}
			}
			Recycle(found_node);
			return true;
		}

		struct iterator
		{
			using difference_type = std::make_signed_t<std::size_t>;
			using value_type = RbTreeValue<K, T>;
			using reference = RbTreeValue<K const &, T const &>;
			using pointer = void;

			iterator operator++(int) noexcept
			{
				auto out = *this;
				++*this;
				return out;
			}
			iterator operator--(int) noexcept
			{
				auto out = *this;
				--*this;
				return out;
			}
			iterator& operator++() noexcept
			{
				id = tree->MoveDir(id, dir);
				return *this;
			}
			iterator& operator--() noexcept
			{
				id = tree->MoveDir(id, GetOtherSide(dir));
				return *this;
			}
			reference operator*() const noexcept
			{
				return reference{ tree->GetKey(id), tree->GetValue(id) };
			}

			friend bool operator==(iterator const & lhs, iterator const & rhs) noexcept
			{
				return lhs.id == rhs.id;
			}

			friend bool operator!=(iterator const & lhs, iterator const & rhs) noexcept
			{
				return not (lhs == rhs);
			}

			RbNodeSide dir = RbNodeSide::Right;
			rb_node_id id = rb_npos;
			RbTree const * tree = nullptr;
		};
		static_assert(std::input_iterator<iterator>);

		[[nodiscard]] constexpr auto begin() const noexcept -> iterator
		{
			return iterator{ RbNodeSide::Right, GetExtreme(m_root, RbNodeSide::Left), this };
		}

		[[nodiscard]] constexpr auto end() const noexcept -> iterator
		{
			return iterator{ RbNodeSide::Right, rb_npos, this };
		}

		[[nodiscard]] constexpr auto rbegin() const noexcept -> iterator
		{
			return iterator{ RbNodeSide::Left, GetExtreme(m_root, RbNodeSide::Right), this };
		}

		[[nodiscard]] constexpr auto rend() const noexcept -> iterator
		{
			return iterator{ RbNodeSide::Left, rb_npos, this };
		}

		[[nodiscard]] constexpr auto Size() const noexcept -> std::size_t
		{
			return std::distance(begin(), end());
		}

		[[nodiscard]] constexpr auto MaxDepth() const noexcept -> std::size_t
		{
			return GetDepth(m_root, 0);
		}

		[[nodiscard]] auto ToString() const noexcept -> std::string
		{
			std::string output;
			output = "flowchart TD\n";
			output += "\tclassDef Red fill:#f00\n";
			output += "\tclassDef Black fill:#000,color:#fff\n";
			output += "\tclassDef Null display: none;\n";

			auto to_key_string = [&](node_id id)
			{
				return ((id == m_root) ? "root:": "") + std::to_string(GetKey(id));
			};
			auto to_basic_string = [&](node_id id)
			{
				return to_key_string(id);
			};
			auto unique_null_node = [v(0)]() mutable
			{
				return std::to_string(v++) + "null:::Null";
			};
			
			auto edge_from_to = [&](node_id from, node_id to)
			{
				return to_key_string(from) + " --> " + to_key_string(to);
			};
			
			auto labelled_edge_from_to = [&](node_id from, std::string label, node_id to)
			{
				return to_key_string(from) + " -->|" + label + "| " + to_key_string(to);
			};
			
			auto dotted_edge_from_to = [&](node_id from, node_id to)
			{
				return to_key_string(from) + " -.-> " + to_key_string(to);
			};
			
			auto labelled_dotted_edge_from_to = [&](node_id from, std::string label, node_id to)
			{
				return to_key_string(from) + " -.->|" + label + "| " + to_key_string(to);
			};
			
			auto dotted_edge_from_to_nothing = [&](node_id from)
			{
				return to_key_string(from) + " ~~~ " + unique_null_node();
			};

			for (node_id id = 0; id < m_container.size(); ++id)
			{
				output += "\t" + to_basic_string(id) + ":::" + std::string((m_container[id].colour == RbColour::Red) ? "Red" : "Black") + "\n";
			}

			// Print parents.
			// for (node_id id = 0; id < m_container.size(); ++id)
			// {
			// 	auto const p = GetParent(id);
			// 	if (p != rb_npos)
			// 	{
			// 		output += "\t" + dotted_edge_from_to(id, p) + "\n";
			// 	}
			// }
			for (node_id id = 0; id < m_container.size(); ++id)
			{
				auto const l = GetChild(id, RbNodeSide::Left);
				auto const r = GetChild(id, RbNodeSide::Right);
				if (l != rb_npos)
				{
					output += "\t" + labelled_edge_from_to(id, "left", l) + "\n";
				}
				else
				{
					output += "\t" + dotted_edge_from_to_nothing(id) + "\n";
				}
				if (r != rb_npos)
				{
					output += "\t" + labelled_edge_from_to(id, "right", r) + "\n";
				}
				else
				{
					output += "\t" + dotted_edge_from_to_nothing(id) + "\n";
				}
			}
			output += "\tsubgraph Recycling\n";
			for (auto node : m_recycle_nodes)
			{
				output += "\t\t" + to_basic_string(node) + "\n";
			}
			output += "\tend\n";
			return output + "\n";
		}
	private:
		[[gnu::always_inline]] inline constexpr void FixAfterInsert(node_id x) noexcept
		{
			for (;;)
			{
				auto p = GetParent(x);
				if (IsBlack(p)) { break; }
				auto const g = GetParent(p);
				if (IsNull(g))
				{
					SetBlack(p);
					break;
				}
				auto const parents_side = GetSideOf(g, p);
				auto const u = GetChild(g, GetOtherSide(parents_side));
				if (IsRed(u))
				{
					SetBlack(p);
					SetBlack(u);
					SetRed(g);
					x = g;
				}
				else
				{
					auto const nodes_side = GetSideOf(p, x);
					if (parents_side != nodes_side)
					{
						Rotate(p, parents_side);
						p = x;
					}
					Rotate(g, GetOtherSide(parents_side));
					SetBlack(p);
					SetRed(g);
					break;
				}
			}
			SetBlack(m_root);
		}

		[[gnu::always_inline]] inline constexpr void FixAfterRemove(node_id parent, node_id x, RbNodeSide x_side) noexcept
		{
			while (not IsRoot(x) and IsBlack(x))
			{
				auto sibling = GetOtherChild(parent, x_side);
				if (IsRed(sibling))
				{
					SetBlack(sibling);
					SetRed(parent);
					sibling = Rotate(parent, x_side);
				}
				auto const left_sibling_child = GetLeftChild(sibling);
				auto const right_sibling_child = GetRightChild(sibling);
				if (IsBlack(left_sibling_child) and IsBlack(right_sibling_child))
				{
					SetRed(sibling);
					x = parent;
					parent = GetParent(parent);
				}
				else
				{
					if (IsBlack(right_sibling_child))
					{
						SetBlack(left_sibling_child);
						SetRed(sibling);
						sibling = Rotate(sibling, GetOtherSide(x_side));
					}
					SetColour(sibling, GetColour(parent));
					SetBlack(parent);
					SetBlack(right_sibling_child);
					Rotate(parent, x_side);
					x = m_root;
				}
			}
			SetBlack(x);
		}

		[[gnu::always_inline]] inline constexpr void FixAfterRemove(node_id x) noexcept
		{
			auto const p = GetParent(x);
			FixAfterRemove(p, x, GetSideOf(p, x));
		}

		[[nodiscard, gnu::always_inline]] inline constexpr auto GetDepth(node_id cursor, std::size_t count = 0) const noexcept -> std::size_t
		{
			auto explore_side = [&](RbNodeSide node_side)
			{
				auto const temp = GetChild(cursor, node_side);
				if (IsValid(temp))
				{
					return GetDepth(temp, count + 1);
				}
				return count;
			};
			return std::max(explore_side(RbNodeSide::Left), explore_side(RbNodeSide::Right));
		}

		[[nodiscard, gnu::always_inline]] inline constexpr auto GetExtreme(node_id cursor, RbNodeSide dir) const noexcept -> node_id
		{
			node_id output = cursor;
			for (;;)
			{
				auto const next = GetChild(output, dir);
				if (next == rb_npos) return output;
				output = next;
			}
		}

		[[nodiscard, gnu::always_inline]] inline constexpr auto MoveDir(node_id cursor, RbNodeSide dir) const noexcept -> node_id
		{
			HMV_ASSERT(IsValid(cursor));
			auto const dir_child = GetChild(cursor, dir);
			if (IsValid(dir_child))
			{
				return GetExtreme(dir_child, GetOtherSide(dir));
			}
			// No dir child, this means we need to go up.
			auto const p = GetParent(cursor);
			if (IsValid(p))
			{
				if (dir != GetSideOf(p, cursor))
				{
					return p;
				}
				for (auto i = p; IsValid(i); i = GetParent(i))
				{
					auto const g = GetParent(i);
					if (IsValid(g) and
						(dir != GetSideOf(g, i)))
					{
						return g;
					}
				}
			}
			return rb_npos;
		}

		struct FindNodeResult
		{
			RbNodeSide found_side = RbNodeSide::Left;
			node_id found_node = rb_npos;
			RbNodeSide parent_side = RbNodeSide::Left;
			node_id parent_node = rb_npos;
		};

		[[nodiscard, gnu::always_inline]] inline constexpr auto FindNode(K const & key, node_id cursor) const noexcept -> FindNodeResult
		{
			FindNodeResult output{ .found_node = cursor };
			while (output.found_node != rb_npos)
			{
				auto const & cursor_key = GetKey(output.found_node);
				if (cursor_key < key)
				{
					output.parent_side = output.found_side;
					output.parent_node = output.found_node;
					output.found_side = RbNodeSide::Right;
				}
				else if (key < cursor_key)
				{
					output.parent_side = output.found_side;
					output.parent_node = output.found_node;
					output.found_side = RbNodeSide::Left;
				}
				else
				{
					break;
				}
				output.found_node = GetChild(output.found_node, output.found_side);
			}
			return output;
		}
		/**
		 * Example: rot C right -->
		 *  +---------+---------+
		 *  |   O     | O       |
		 *  |    \    |  \      |
		 *  |     e   |   c     
		 *  |    / \  |  / \    |
		 *  |   c   g | a   e   
		 *  |  / \    |    / \  |
		 *  | a   d   |   d   g |
		 *  +---------+---------+
		 */
		[[gnu::always_inline]] inline constexpr auto Rotate(node_id e, RbNodeSide dir) noexcept -> node_id
		{
			auto o = GetParent(e);
			auto c = GetOtherChild(e, dir);
			auto d = GetChild(c, dir);
			SetParent(c, o);
			if (IsValid(o)) SetChild(o, GetSideOf(o, e), c);
			SetParent(e, c);
			SetChild(c, dir, e);
			if (IsValid(d)) SetParent(d, e);
			SetOtherChild(e, dir, d);
			return c;
		}
		[[nodiscard, gnu::always_inline]] inline constexpr bool IsBlack(node_id id) const noexcept
		{
			return (GetColour(id) == RbColour::Black);
		}
		[[nodiscard, gnu::always_inline]] inline constexpr std::size_t ChildCount(node_id id) const noexcept
		{
			return std::size_t(GetLeftChild(id) != rb_npos) + std::size_t(GetRightChild(id) != rb_npos);
		}
		[[nodiscard, gnu::always_inline]] inline constexpr bool IsLeaf(node_id id) const noexcept
		{
			return ChildCount(id) == 0;
		}
		[[nodiscard, gnu::always_inline]] inline constexpr bool IsRed(node_id id) const noexcept
		{
			return (GetColour(id) == RbColour::Red);
		}
		[[gnu::always_inline]] inline constexpr void SetBlack(node_id id) noexcept
		{
			if (id == rb_npos) return; // already black.
			SetColour(id, RbColour::Black);
		}
		[[gnu::always_inline]] inline constexpr void SetRed(node_id id) noexcept
		{
			SetColour(id, RbColour::Red);
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto GetSideOf(node_id id, node_id child) const noexcept -> RbNodeSide
		{
			HMV_ASSERT(IsValid(id));
			HMV_ASSERT(IsValid(child));
			return (child == GetChild(id, RbNodeSide::Left)) ? RbNodeSide::Left : RbNodeSide::Right;
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto IsNull(node_id node) const noexcept -> bool
		{
			return node == rb_npos;
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto IsValid(node_id node) const noexcept -> bool
		{
			return node != rb_npos;
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto IsValid(RbNodeSide dir) const noexcept -> bool
		{
			return (dir == RbNodeSide::Left or dir == RbNodeSide::Right);
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto GetParent(node_id node) const noexcept -> node_id
		{
			HMV_ASSERT(IsValid(node));
			return m_container[node].parent;
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto GetLeftChild(node_id node) const noexcept -> node_id
		{
			return GetChild(node, RbNodeSide::Left);
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto GetRightChild(node_id node) const noexcept -> node_id
		{
			return GetChild(node, RbNodeSide::Right);
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto GetChild(node_id node, RbNodeSide dir) const noexcept -> node_id
		{
			HMV_ASSERT(IsValid(node));
			HMV_ASSERT(IsValid(dir));
			return m_container[node].children[static_cast<node_id>(dir)];
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto GetOtherChild(node_id node, RbNodeSide dir) const noexcept -> node_id
		{
			HMV_ASSERT(IsValid(node));
			HMV_ASSERT(IsValid(dir));
			return m_container[node].children[static_cast<node_id>(GetOtherSide(dir))];
		}
		[[gnu::always_inline]] inline constexpr void ClearChild(node_id node, RbNodeSide dir) noexcept
		{
			HMV_ASSERT(IsValid(node));
			HMV_ASSERT(IsValid(dir));
			m_container[node].children[static_cast<node_id>(dir)] = rb_npos;
		}
		[[gnu::always_inline]] inline constexpr void SetChild(node_id node, RbNodeSide dir, node_id id) noexcept
		{
			HMV_ASSERT(IsValid(dir));
			if (IsNull(node)) { SetRoot(id); }
			else { m_container[node].children[static_cast<node_id>(dir)] = id; }
		}
		[[gnu::always_inline]] inline constexpr void SetOtherChild(node_id node, RbNodeSide dir, node_id id) noexcept
		{
			SetChild(node, GetOtherSide(dir), id);
		}
		[[gnu::always_inline]] inline constexpr void SetRoot(node_id node) noexcept
		{
			HMV_ASSERT(IsValid(node));
			m_root = node;
		}
		[[gnu::always_inline]] inline constexpr void Link(node_id parent, node_id child, RbNodeSide child_side)
		{
			SetChild(parent, child_side, child);
			SetParent(child, parent);
		}
		[[gnu::always_inline]] inline constexpr void SetParent(node_id node, node_id id) noexcept
		{
			HMV_ASSERT(IsValid(node));
			m_container[node].parent = id;
		}

		struct RbNode
		{
			K key;
			RbColour colour = RbColour::Red;
			rb_node_id parent = rb_npos;
			rb_node_id children[2] = { rb_npos, rb_npos }; ///< Index with RbNodeSide.
		};

		using node_type = RbNode;
		using storage_type = std::vector<node_type, Alloc<node_type>>;
		using keys_type = std::vector<K, Alloc<K>>;
		using colours_type = std::vector<RbColour, Alloc<K>>;
		using values_type = std::vector<T, Alloc<T>>;

		[[nodiscard, gnu::always_inline]] inline constexpr auto GetNode(node_id id) noexcept -> node_type &
		{
			HMV_ASSERT(IsValid(id));
			return m_container[id]; 
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto GetNode(node_id id) const noexcept -> node_type const &
		{
			HMV_ASSERT(IsValid(id));
			return m_container[id]; 
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto GetKey(node_id id) const noexcept -> K const &
		{
			HMV_ASSERT(IsValid(id));
			return m_container[id].key;
		}
		[[nodiscard]] inline constexpr auto IsChildOf(node_id parent, node_id child) const noexcept -> bool
		{
			return GetLeftChild(parent) == parent or GetRightChild(parent) == child;
		}
		[[nodiscard]] inline constexpr auto IsRoot(node_id id) const noexcept -> bool
		{
			return id == m_root;
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto GetValue(node_id id) const noexcept -> T const &
		{
			HMV_ASSERT(IsValid(id));
			return m_values[id];
		}
		[[nodiscard, gnu::always_inline]] inline constexpr auto GetColour(node_id id) const noexcept -> RbColour
		{
			return IsNull(id) ? RbColour::Black : m_container[id].colour;
		}
		[[gnu::always_inline]] inline constexpr void SetColour(node_id id, RbColour colour) noexcept
		{
			HMV_ASSERT(IsValid(id));
			m_container[id].colour = colour;
		}
		[[gnu::always_inline]] inline constexpr auto NewNode(K const & key, T const & value, RbColour colour = RbColour::Red) -> std::size_t
		{
			if (m_recycle_nodes.empty())
			{
				auto const output = m_container.size();
				m_container.push_back({ .key = key, .colour = colour });
				m_values.push_back(value);
				return output;
			}
			else
			{
				auto const output = m_recycle_nodes.back();
				m_recycle_nodes.pop_back();
				m_container[output] = { .key = key, .colour = colour };
				m_values[output] = value;
				return output;
			}
		}

		[[gnu::always_inline]] inline constexpr auto Recycle(node_id id)
		{
#ifndef NDEBUG
			SetParent(id, rb_npos);
			SetChild(id, RbNodeSide::Left, rb_npos);
			SetChild(id, RbNodeSide::Right, rb_npos);
#endif
			m_recycle_nodes.push_back(id);
		}

		node_id m_root = rb_npos;
		storage_type m_container;
		std::vector<node_id> m_recycle_nodes;
		values_type m_values;
	};
}

namespace std
{
    template <typename...Ts, typename...Us, template<typename> class Qual1, template<typename> class Qual2>
		requires (std::common_reference_with<Us, Ts> and ...)
    struct basic_common_reference<::Hmv::RbTreeValue<Ts...>, ::Hmv::RbTreeValue<Us...>, Qual1, Qual2>
    {
		using type = ::Hmv::RbTreeValue<std::common_reference_t<Qual1<Ts>, Qual2<Us>>...>;
	};
}