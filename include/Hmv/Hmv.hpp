#pragma once
// NOLINTBEGIN
#include "Hmv/FindHeaders.hpp"
#include "Hmv/ParseCommon.hpp"
// NOLINTEND
/**
 * @brief The header move namespace.
 */
namespace Hmv{}