#include "Hmv/FindHeaders.hpp"
#include <optional>
#include <algorithm>

namespace Hmv
{
	namespace
	{
		auto is_any( auto c, auto ... matches )
		{
			return ( ( c == matches ) or ... );
		}

		auto is_h_char( char c ) -> bool
		{
			return not is_any( c, u8'\n', u8'\r', u8'>' );
		}

		auto is_q_char( char c ) -> bool
		{
			return not is_any( c, u8'\n', u8'\r', u8'"' );
		}

		auto is_newline( char c ) -> bool
		{
			return is_any( c, u8'\r', u8'\n' );
		}

		auto is_space( char c ) -> bool
		{
			return is_any( c, u8' ', u8'\t' );
		}

		auto is_whitespace( char c ) -> bool
		{
			return is_any( c, u8' ', u8'\t', u8'\r', u8'\n' );
		}

		auto parse_chars( string_view_type input, auto match ) -> ParseResult<string_view_type>
		{
			std::size_t i = 0;
			for ( ; i < input.size(); ++i )
			{
				auto const c = input[i];
				if ( not match( c ) ) { break; }
			}
			if ( i == 0 ) return {};
			return ParseResult<string_view_type>{
				.remaining = input.substr(i),
				.artifact = input.substr(0, i),
			};
		}

		template<char c>
		auto parse_char( string_view_type input ) -> ParseResult<string_view_type>
		{
			if ( input.empty() ) return {};
			if ( input.front() != c ) return {};
			return ParseResult<string_view_type>{
				input.substr( 1 ),
				input.substr( 0, 1 ),
			};
		}

		auto parse_string( string_view_type input, string_view_type match ) -> ParseResult<string_view_type>
		{
			if ( input.empty() ) return {};
			if ( not input.starts_with(match) ) return {};
			return ParseResult<string_view_type>{
				input.substr(match.size()),
				input.substr(0, match.size()),
			};
		}

		auto parse_spaces( string_view_type input ) -> ParseResult<string_view_type>
		{
			return parse_chars( input, is_space );
		}

		auto parse_newline( string_view_type input ) -> ParseResult<string_view_type>
		{
			return parse_chars( input, is_newline );
		}

		auto parse_whitespace( string_view_type input ) -> ParseResult<string_view_type>
		{
			return parse_chars( input, is_whitespace );
		}

		auto parse_opt_spaces( string_view_type input ) -> ParseResult<string_view_type>
		{
			auto const spaces = parse_spaces( input );
			if ( spaces.IsValid() ) { return spaces; }
			return ParseResult<string_view_type>{
				.remaining = input,
				.artifact = ""
			};
		}

		auto parse_opt_whitespace( string_view_type input ) -> ParseResult<string_view_type>
		{
			auto const whitespace = parse_whitespace( input );
			if ( whitespace.IsValid() ) { return whitespace; }
			return ParseResult<string_view_type>{
				.remaining = input,
				.artifact = ""
			};
		}

		auto get_whole_line( string_view_type source, std::size_t pos ) -> string_view_type
		{
			std::size_t stop_i = pos;
			for (; stop_i < source.size(); ++stop_i)
			{
				auto const c = source[stop_i];
				if (is_newline(c))
				{
					break;
				}
			}
			std::make_signed_t<std::size_t> start_i = pos;
			while(start_i > 0)
			{
				auto const c = source[--start_i];
				if (is_newline(c))
				{
					++start_i;
					break;
				}
			}
			auto const length = stop_i - start_i;
			return string_view_type(source.data() + start_i, length);
		}

		auto const include_header = string_view_type("#include");
	}

	auto HeaderSearchOptions::IsOpenChar(string_view_type::value_type input) const noexcept
	{
		if (include_quoted)
		{
			if (input == u8'"') return true;
		}
		if (include_angle_brackets)
		{
			if (input == u8'<') return true;
		}
		return false;
	}

	auto HeaderSearchOptions::GetCloseChar(string_view_type::value_type open_char) const noexcept
	{
		if (open_char == u8'"') return u8'"';
		if (open_char == u8'<') return u8'>';
		return u8'\0';
	}

	auto HeaderSearchOptions::IsAllowedChar(string_view_type::value_type open_char, string_view_type::value_type input) const noexcept
	{
		if (open_char == u8'"') return is_q_char(input);
		if (open_char == u8'<') return is_h_char(input);
		return false;
	}

	auto ParseIncludeString(string_view_type input, HeaderSearchOptions const & options) -> ParseIncludeResult
	{
		if ( input.empty() ) return {};
		auto const open_char = input.front();

		// Test the first character if it is valid
		// -- if it is, configure the closing match character.
		if (not options.IsOpenChar(open_char)) return {};
		auto const required_close_char = options.GetCloseChar(open_char);
		auto matcher = [&](string_view_type::value_type c)
		{
			return options.IsAllowedChar(open_char, c);
		};
		
		auto const opt_h_char_count = parse_chars( input.substr( 1 ), matcher );
		if ( not opt_h_char_count.IsValid() ) return {};
		auto const h_char_count = opt_h_char_count.artifact.size() + 1;
		if ( h_char_count >= input.size() ) return {};
		auto const close_char = input[h_char_count];
		if ( close_char != required_close_char ) return {};
		auto const end_length = h_char_count + 1;
		return ParseIncludeResult{
			.remaining = input.substr( end_length ),
			.artifact = input.substr( 0, end_length ),
		};
	}

	auto ParseInclude(string_view_type input, HeaderSearchOptions const & options) -> ParseIncludeResult
	{
		// |     #include     "header.h"     
		//    ^            ^              ^  
		//    a            b              c  
		// Parse section a.
		auto const a = parse_opt_whitespace(input);
		// Parse '#include'.
		auto hash_include = parse_string(a.remaining, include_header);
		if (not hash_include.IsValid()) return ParseIncludeResult{};
		// Parse section b.
		auto b_string = parse_opt_whitespace(hash_include.remaining);
		// Parse '"header.h"'
		auto header_name = ParseIncludeString(b_string.remaining, options);
		if (not header_name.IsValid()) return {};
		// Parse section c.
		auto const trailing_spaces = parse_opt_spaces(header_name.remaining);
		return ParseIncludeResult{
			.remaining = trailing_spaces.remaining,
			.artifact = header_name.artifact,
		};
	}

	auto FindFirstInclude( string_view_type input, HeaderSearchOptions const & options) -> ParseIncludeResult
	{
		auto start_it = input.begin();
		auto const end_it = input.end();
		while (start_it != end_it)
		{
			start_it = std::search(start_it, end_it, include_header.begin(), include_header.end());
			if ( start_it == end_it ) return {};

			auto const found_index = std::size_t(start_it - input.begin());
			// Ensure this is the only thing on that line.
			auto const line = get_whole_line(input, found_index);
			auto const parse_line_result = ParseInclude(line);
			if (not parse_line_result.IsValid())
			{
				start_it += include_header.size();
				continue;
			}
			return {
				.remaining = { parse_line_result.remaining.data(), input.data() + input.size() },
				.artifact = parse_line_result.artifact,
			};
		}
		return {};
	}

	auto FindAllIncludes(string_view_type contents, HeaderSearchOptions const & options) -> HeaderList
	{
		HeaderList output;
		auto first_include = Hmv::FindFirstInclude(contents, options);
		while (first_include.IsValid())
		{
			auto [remaining, artifact] = first_include;
			if (not artifact.empty())
			{
				auto const c = artifact.front();
				if (c == '"') { output.push_back(artifact); }
				else if (c == '<') { output.push_back(artifact); }
				else { return {}; }
			}
			first_include = Hmv::FindFirstInclude(remaining);
		}
		return output;
	}
}