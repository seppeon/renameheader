#include "Hmv/PieceTable.hpp"
#include <cstring>
#include <span>
#include <algorithm>
#include <cassert>

namespace Hmv
{
	namespace
	{
		inline constexpr std::size_t find_npos = ~std::size_t();

		struct InsertionIndexResult
		{
			std::size_t index = find_npos;
			std::size_t offset = find_npos;
		};

		constexpr auto FindNode(std::span<PieceTableNode const> nodes, std::size_t index, std::size_t min_index = 0, std::size_t current = 0) noexcept -> InsertionIndexResult
		{
			for (std::size_t i = min_index; i < nodes.size(); ++i)
			{
				auto const & node = nodes[i];
				auto const next = current + node.length;
				if (next > index)
				{
					return {
						.index = i,
						.offset = current,
					};
				}
				current = next;
			}
			return {};
		}

		constexpr auto test_nodes = std::array{
			PieceTableNode{
				.start = 0,
				.length = 23,
			},
			PieceTableNode{
				.start = 40,
				.length = 10,
			}
		};
		static_assert(FindNode(test_nodes, 0).index == 0);          // start of range.
		static_assert(FindNode(test_nodes, 20).index == 0);         // mid of range.
		static_assert(FindNode(test_nodes, 22).index == 0);         // end of range.
		static_assert(FindNode(test_nodes, 23).index == 1);         // next range.
		static_assert(FindNode(test_nodes, 25).index == 1);         // mid next range.
		static_assert(FindNode(test_nodes, 32).index == 1);         // next range.
		static_assert(FindNode(test_nodes, 33).index == find_npos); // out of range.
	}

	PieceTable::PieceTable(container_type data)
		: m_source_length{ data.size() }
		, m_total_length{ data.size() }
		, m_data{ std::move(data) }
		, m_nodes{ PieceTableNode{
			.start = 0,
			.length = m_source_length
		} }
	{}

	auto PieceTable::OriginalContent() const noexcept -> string_view_type
	{
		return string_view_type{ m_data.data(), m_source_length };
	}

	auto PieceTable::Size() const noexcept -> std::size_t
	{
		return m_total_length;
	}

	auto PieceTable::SumOfLengths() const noexcept -> std::size_t
	{
		std::size_t output = 0;
		for (auto const & node : m_nodes)
		{
			output += node.length;
		}
		return output;
	}

	auto PieceTable::operator[](std::size_t index) const noexcept -> string_type::const_reference
	{
		auto const [node_index, node_offset] = FindNode(m_nodes, index);
		auto const & node = m_nodes[node_index];
		return m_data[node.start + (index - node_offset)];
	}

	void PieceTable::Append(string_view_type new_data)
	{
		m_nodes.push_back({ PieceTableNode{
			.start = m_data.size(),
			.length = new_data.size()
		} });
		m_data.append(new_data);
		m_total_length += new_data.size();
	}

	void PieceTable::Replace(PieceTableNode old_range, string_view_type new_data)
	{
		if (new_data.empty()) 
		{
			// This should have been a delete operation.
			return Delete(old_range);
		}
		if (old_range.length == 0)
		{
			// This should have been an insert operation.
			return Insert(old_range.start, new_data);
		}
		auto const adjusted_end = std::min(old_range.start + old_range.length, Size());
		old_range = PieceTableNode{
			.start = old_range.start,
			.length = adjusted_end - old_range.start
		};

		// We have a non-empty range, and non-empty new data.
		auto const new_node = PieceTableNode{
			.start = m_data.size(),
			.length = new_data.size(),
		};
		auto const [old_start, old_length] = old_range;
		auto const old_end = old_start + old_length;
		m_data.append(new_data);
		m_total_length -= old_length;
		m_total_length += new_data.size();

		auto const new_start = old_range.start;
		auto const new_length = new_data.size();
		auto const [lhs_node_index, lhs_node_start] = FindNode(m_nodes, old_start);
		auto const [rhs_node_index, rhs_node_start] = FindNode(m_nodes, old_end, lhs_node_index, lhs_node_start);
		assert(lhs_node_index != find_npos);
		if (lhs_node_index == rhs_node_index)
		{
			/**
			 * In this case, we are within a single node there are
			 * three special cases here, these are outlined in each branch.
			 */
			auto & node = m_nodes[lhs_node_index];
			auto const lhs_start  = node.start;
			auto const lhs_length = old_start - lhs_node_start;
			auto const rhs_start = lhs_start + lhs_length + old_range.length;
			auto const rhs_length = node.length - old_range.length - lhs_length;

			if (lhs_length == 0)
			{
				if (rhs_length == 0)
				{
					/**
					 * lhs and rhs nodes both have a length of zero
					 *     |<--------- `current_node` --------->|
					 *     |<----------- `new_data` ----------->|
					 * In this case:
					 * - the `current_node` is recycled, and used for `new_data`
					 * 
					 * Insertion operations: 0
					 * Insertion length: 0
					 */
					node = new_node;
					assert(Size() == SumOfLengths());
				}
				else
				{
					/**
					 * lhs node has a length of zero.
					 *     |<----------- current node ----------->|
					 *     |<-------- new_data -------->|<--rhs-->|
					 *                                  ^
					 *                                  split
					 * In this case:
					 * - the current node is recycled, and used for `new_data`.
					 * - the rhs node is inserted as new data, in a single operation.
					 * 
					 * Insertion operations: 1
					 * Insertion length: 1
					 */
					node = new_node;
					m_nodes.insert(m_nodes.begin() + lhs_node_index + 1, {
						PieceTableNode{
							.start = rhs_start,
							.length = rhs_length,
						}
					});
					assert(Size() == SumOfLengths());
				}
			}
			else
			{
				if (rhs_length == 0)
				{
					/**
					 * rhs node has a length of zero
					 *     |<----------- current node ----------->|
					 *     |<--lhs-->|<-------- new_data -------->|
					 *               ^
					 *               split
					 * In this case:
					 * - the current node is recycled, and used for `lhs`.
					 * - the rhs node is inserted as new data, in a single operation.
					 * 
					 * Insertion operations: 1
					 * Insertion length: 1
					 */
					node = new_node;
					m_nodes.insert(m_nodes.begin() + lhs_node_index, {
						PieceTableNode{
							.start = lhs_start,
							.length = lhs_length,
						}
					});
					assert(Size() == SumOfLengths());
				}
				else
				{
					/**
					 *     |<----------- current node ----------->|
					 *     |<--lhs-->|<--- new_data --->|<--rhs-->|
					 *               ^                  ^
					 *               split              split
					 * In this case:
					 * - the current node is recycled, and used for `lhs`.
					 * - the new node and rhs are inserted as new data, in a single operation.
					 * 
					 * Insertion operations: 1
					 * Insertion length: 2
					 */
					node = PieceTableNode{
						.start = lhs_start,
						.length = lhs_length,
					};
					m_nodes.insert(m_nodes.begin() + lhs_node_index + 1, {
						new_node,
						PieceTableNode{
							.start = rhs_start,
							.length = rhs_length,
						}
					});
					assert(Size() == SumOfLengths());
				}
			}
		}
		else if (rhs_node_index != find_npos)
		{
			/**
			 *    |<------- node 1 ------->|<------- node 2 -------->|<------- node 3 ------->|
			 *    |<--- `lhs` --->|<-------------- `new_data` --------------->|<--- `rhs` --->|
			 *                    ^                                           ^
			 *                    split                                       split
			 * 
			 * Note: When there are more than 2 nodes involved, the 3 required nodes can always
			 *       be recycled, avoid an allocation. This is a very desirable situation.
			 */
			/**
			 *    |<------- node 1 ------->|<------- node 2 -------->|<------- node n ------->|
			 *    ^lhs_node_start          |                         ^node_n_start            |
			 *    |<--- `node_1_length` -->|                         |<--- `node_n_length`--->|
			 */
			auto & node_1 = m_nodes[lhs_node_index];
			auto & node_n = m_nodes[rhs_node_index];

			// 	node_1's start is pinned, calculations are relative to that point.
			auto const node_1_start = node_1.start;
			auto const node_1_length = new_start - lhs_node_start;

			// node_n's end is pinned, calculations are relative to that point.
			auto const new_data_end = new_start + old_length;
			auto const node_n_overlap = new_data_end - rhs_node_start;
			auto const node_n_offset = node_n.length - node_n_overlap;
			auto const node_n_end = node_n.start + node_n.length;
			auto const node_n_start = node_n_end - node_n_offset;
			auto const node_n_length = node_n_offset;

			// Recycle the front and back nodes, we know they exist at this point.
			node_1 = PieceTableNode{
				.start = node_1_start,
				.length = node_1_length,
			};
			node_n = PieceTableNode{
				.start = node_n_start,
				.length = node_n_length,
			};
			if (lhs_node_index + 1 == rhs_node_index)
			{
				// This case has no nodes avaliable, time for the slowpoke path.
				m_nodes.insert(m_nodes.begin() + lhs_node_index + 1, new_node);
				assert(Size() == SumOfLengths());
			}
			else
			{
				// This case has recyclable nodes, lets just use the first one,
				// that way we *slightly* avoid fragmentation of empty sections.
				m_nodes[lhs_node_index + 1] = new_node;
				// Any excess nodes need to be cleared, the nodes will be left in place.
				// they can be recycled some other time, hotspots in the container 
				// are likely to remain hotspots, making this an optimisation.
				for (std::size_t i = lhs_node_index + 2; i < rhs_node_index; ++i)
				{
					m_nodes[i].length = 0;
				}
				assert(Size() == SumOfLengths());
			}
		}
		else
		{
			/**
			 * |<----------- old node ------------>|
			 * |<-- `lhs` -->|<---- `new_data` -----
			 *               ^
			 *               split
			 */
			auto & node = m_nodes[lhs_node_index];
			auto const lhs_start = node.start;
			auto const lhs_length = old_range.start - lhs_node_start;
			node = PieceTableNode{
				.start = lhs_start,
				.length = lhs_length,
			};
			m_nodes.insert(m_nodes.begin() + lhs_node_index + 1, new_node);
			// Its possible other items exist, they are all zeroed.
			for (std::size_t i = lhs_node_index + 2; i < m_nodes.size(); ++i)
			{
				m_nodes[i].length = 0;
			}
			assert(Size() == SumOfLengths());
		}
	}

	void PieceTable::Delete(PieceTableNode range)
	{
		// Deleting nothing is easy, done.
		if (range.length == 0) { return; }
		auto const range_start = range.start;
		auto const range_end = range.start + range.length;
		auto const [lhs_index, lhs_start] = FindNode(m_nodes, range_start);
		auto const [rhs_index, rhs_start] = FindNode(m_nodes, range_end, lhs_index, lhs_start);
		assert(lhs_index != find_npos);
		if (lhs_index == rhs_index)
		{
			// This is inside just one node, adjust that node, splitting it.
			auto & node = m_nodes[lhs_index];
			auto const after_node_it = m_nodes.begin() + lhs_index + 1;
			/**
			 * |<--lhs-->|<--removed data-->|<--rhs-->|
			 */
			auto const new_lhs_start  = node.start;
			auto const new_lhs_length = range.start - lhs_start;
			auto const rm_start       = new_lhs_start + new_lhs_length;
			auto const rm_length      = range.length;
			auto const new_rhs_start  = rm_start + rm_length;
			auto const new_rhs_length = node.length - rm_length - new_lhs_length;
			assert(new_lhs_length <= m_total_length);
			assert(new_rhs_length <= m_total_length);
			assert(rm_length <= m_total_length);
			node = PieceTableNode{
				.start = new_lhs_start,
				.length = new_lhs_length,
			};
			m_nodes.insert(after_node_it,
				PieceTableNode{
					.start = new_rhs_start,
					.length = new_rhs_length,
				});
		}
		else if (rhs_index != find_npos)
		{
			/**
			 *                   |           |
			 * |<--lhs-->|<--remo|   nodes   |ved data-->|<--rhs-->|
			 *                   |           |
			 */
			auto & lhs_node = m_nodes[lhs_index];
			auto & rhs_node = m_nodes[rhs_index];
			auto const lhs_node_start = lhs_node.start;
			auto const lhs_node_length = range.start - lhs_start;
			auto const rhs_overlap = (range_end - rhs_start);
			auto const rhs_node_start = rhs_node.start + rhs_overlap;
			auto const rhs_node_length = rhs_node.length - rhs_overlap;
			for (auto i = lhs_index + 1; i < rhs_index; ++i)
			{
				m_nodes[i].start = range.start;
				m_nodes[i].length = 0;
			}
			assert(lhs_node_length <= m_total_length);
			assert(rhs_node_length <= m_total_length);
			lhs_node = {
				.start = lhs_node_start,
				.length = lhs_node_length,
			};
			rhs_node = {
				.start = rhs_node_start,
				.length = rhs_node_length,
			};
		}
		else
		{
			/**
			 *                   
			 * |<--lhs-->|<--removed data-----
			 *                   
			 */
			auto & lhs_node = m_nodes[lhs_index];
			auto const lhs_node_start = lhs_node.start;
			auto const lhs_node_length = range.start - lhs_start;
			for (auto i = lhs_index + 1; i < m_nodes.size(); ++i)
			{
				m_nodes[i].start = range.start;
				m_nodes[i].length = 0;
			}
			assert(lhs_node_length <= m_total_length);
			lhs_node = {
				.start = lhs_node_start,
				.length = lhs_node_length,
			};
		}
		m_total_length -= range.length;
	}

	void PieceTable::Insert(std::size_t index, string_view_type new_data)
	{
		auto const [selected_node_index, selected_node_start] = FindNode(m_nodes, index);
		if (selected_node_index == find_npos) { return Append(new_data); }

		auto const new_data_start = m_data.size();
		auto const new_data_length = new_data.size();
		auto & selected_node = m_nodes[selected_node_index];
		if (selected_node.length == 0)
		{
			/**
			 * |<--new_data-->|
			 * 
			 * Empty node was found, reuse it.
			 */
			selected_node = PieceTableNode{
				.start = new_data_start,
				.length = new_data_length,
			};
		}
		else
		{
			auto const lhs_start = selected_node.start;
			auto const lhs_length = index - selected_node_start;
			auto const rhs_start = lhs_start + lhs_length;
			auto const rhs_length = selected_node.length - lhs_length;
			// 4 possibilities:
			auto const after_selected_it = m_nodes.begin() + selected_node_index + 1;
			if (lhs_length == 0)
			{
				/**
				 * |<--new_data-->|<--rhs-->|
				 */
				selected_node = PieceTableNode{
					.start = new_data_start,
					.length = new_data_length,
				};
				m_nodes.insert(after_selected_it, {
					PieceTableNode{
						.start = rhs_start,
						.length = rhs_length,
					}
				});
			}
			else
			{
				if (rhs_length == 0)
				{
					/**
					 * |<--lhs-->|<--new_data-->|
					 */
					selected_node = PieceTableNode{
						.start = lhs_start,
						.length = lhs_length,
					};
					m_nodes.insert(after_selected_it, {
						PieceTableNode{
							.start = new_data_start,
							.length = new_data_length,
						}
					});
				}
				else
				{
					/**
					 * |<--lhs-->|<--new_data-->|<--rhs-->|
					 */
					selected_node = PieceTableNode{
						.start = lhs_start,
						.length = lhs_length,
					};
					m_nodes.insert(after_selected_it, {
						PieceTableNode{
							.start = new_data_start,
							.length = new_data_length,
						},
						PieceTableNode{
							.start = rhs_start,
							.length = rhs_length
						},
					});
				}
			}
		}
		m_data.append(new_data);
		m_total_length += new_data.size();
	}

	auto PieceTable::ToString() const noexcept -> string_type
	{
		string_type output;
		output.resize(Size());
		auto * dst = output.data();
		auto const * const src = m_data.data();
		for (auto const & [index, length] : m_nodes)
		{
			std::memcpy(dst, src + index, length);
			dst += length;
		}
		return output;
	}
}