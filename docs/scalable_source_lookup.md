
# Introduction

The size of a source file has a huge dynamic range, from a couple lines to a few hundred thousand, both are common enough to require a scalable solution that serves both scenarios.

# Definitions

1. `Replace`: An operation on the source file for which a range of characters in the current representation of the file are replaced with another range of characters. 
    - The input range can be of zero length, this is equivilent to an insertion, and
    - The new range can be of zero length this is equivilent to a deletion.
2. `Original File Contents`: The contents some file, as stored on disk.
3. `Current File Contents`: The contents of some file, after applying some `Replace` operations (but not yet stored on disk).

# Requirements

When a source file is read by a compiler, its common to have some pre-processing. This can be done with in source modifications (like string operations) when this is the case you want to be able to trace the modified source to the original source. Without this tracability, diagnostics becomes very akward. Given the dynamic range of source file sizes, it is essential that that representation is scalable. Especially for any interactive tool (such as a header renaming tool).

1. A `Replace` operation must be tracable to the `Original File Contents`.

2. The time complexity of a random access operation into a modified or `Original File Contents` must be `O(log N)` or better.

3. The memory complexity of a modified or `Original File Contents` must be `O(N log N)` or better.

4. Given an offset into the `Current File Contents`, the line and column must be resolvable.

5. Given a line and column, the offset into the `Current File Contents` must be resolvable.

# Idea

The original idea to create an abstraction for the representation of source files was inspired from the code by [Peng Lyu @ Microsoft](https://code.visualstudio.com/blogs/2018/03/23/text-buffer-reimplementation). The data structure works applying changes ontop of the original source file. This has several benefits.

A piece table is essentially an array of `Piece`s, which can be traversed in `O( N )` time. However, these pieces only store a length, since storing an offset would force any operation to a `Piece` to update the offset of all proceeding pieces. This leads to a design where you must have some sort of data structure that minimises the impact of a change to any specific `Piece`. What must be solved here is commonly referred to as the prefix sum, and there are several data structures commonly used to represent this efficiently.

The options for data structure here are really dependant on how changes will be applied to the source file, if the changes were to be only applied once, a **Fenwick** tree seems ideal, it is an extremely fast data structure (in space and memory). A fenwick tree can have additional items added over time, but there is a catch, they must be added in order of offset into some file or you will face linear time penalty for an operation (changes can be appended but not arbitarily inserted). A fenwick tree is derived from another data structure called a **Segment Tree**, which has a similar issue with centre insertion, however with some modification this data structure can be made to work with centre insertion.

