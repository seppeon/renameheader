#include <algorithm>
#include <filesystem>
#include <iostream>
#include <memory>
#include <optional>
#include <ranges>
#include <fstream>
#include <set>
#include <span>
#include <streambuf>
#include <string>
#include <string_view>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "Hmv/Hmv.hpp"
#include "hmv/FindHeaders.hpp"
// #include <FL/File.hpp>

namespace
{
	Hmv::ParseResult<int> temp;

	using sz = std::size_t;
	using ssz = std::make_signed_t<sz>;

	auto get_args( int argc, char ** argv ) -> std::vector<std::filesystem::path>
	{
		std::vector<std::filesystem::path> output;
		for ( int i = 1; i < argc; ++i ) output.push_back( { argv[i] } );
		return output;
	}


	auto open_file(std::filesystem::path p) -> Hmv::string_type
	{
		std::ifstream file(p, std::ios_base::in | std::ios_base::binary);
		return { std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>() };
	}

	void save_file(std::filesystem::path p, Hmv::string_type contents)
	{
		std::cout << "Saving from " << p << "\n";
		std::ofstream file(p, std::ios_base::out | std::ios_base::binary);
		file.write(reinterpret_cast<const char *>(contents.data()), contents.size());
	}

	void move_file(std::filesystem::path old_path, std::filesystem::path new_path)
	{
		std::cout << "Rename from " << old_path << " to " << new_path << "\n";
		auto temp_new_path = new_path;
		temp_new_path.remove_filename();
		if (not temp_new_path.empty() and not std::filesystem::exists(temp_new_path))
		{
			std::cout << "Create directory " << temp_new_path << "\n";
			std::filesystem::create_directories(temp_new_path);
		}
		std::filesystem::rename(old_path, new_path);
	}

	struct iterable
	{
		std::filesystem::recursive_directory_iterator b;
		std::filesystem::recursive_directory_iterator e = {};

		iterable(std::filesystem::path p)
			: b{ p }
		{
		}

		auto begin() const noexcept { return b; }
		auto end() const noexcept { return e; }
	};

	struct Include
	{
		std::size_t pos;
		Hmv::string_type header;

		friend bool operator<(Include const & lhs, Include const & rhs) noexcept
		{
			return std::forward_as_tuple(lhs.pos, lhs.header) < std::forward_as_tuple(rhs.pos, rhs.header);
		}
	};

	struct SourceChange
	{
		std::size_t pos;
		std::size_t length;
		Hmv::string_type replacement;

		friend bool operator<(SourceChange const & lhs, SourceChange const & rhs) noexcept
		{
			// The latest changes must be made first.
			return not (lhs.pos < rhs.pos);
		}
	};

	struct Source
	{
		std::filesystem::path old_path;
		std::filesystem::path new_path;
		Hmv::string_type contents;
		std::set<Include> h_includes;
		std::set<Include> q_includes;
		std::set<SourceChange> changes;
	};

	struct HeaderRef
	{
		std::filesystem::path path;
		std::size_t pos;
	};

	struct Header
	{
		std::filesystem::path path;
		std::vector<HeaderRef> refs;
	};

	struct Transformer
	{
		[[nodiscard]] virtual auto Filter(std::filesystem::path p) const noexcept -> bool = 0;

		[[nodiscard]] virtual auto TransformPath(std::filesystem::path p) const noexcept -> std::filesystem::path = 0;

		[[nodiscard]] virtual auto TransformInclude(std::filesystem::path old_path, std::filesystem::path new_path) const noexcept -> std::filesystem::path
		{
			return new_path.lexically_relative(old_path.remove_filename());
		}
	};

	struct ExtensionTransform : Transformer
	{
		[[nodiscard]] auto Filter(std::filesystem::path p) const noexcept -> bool
		{
			return p.extension() == old_extension;
		}

		[[nodiscard]] auto TransformPath(std::filesystem::path p) const noexcept -> std::filesystem::path
		{
			return p.replace_extension(new_extension);
		}
		
		ExtensionTransform(Hmv::string_type old_ext, Hmv::string_type new_ext)
			: old_extension(old_ext)
			, new_extension(new_ext)
		{}
	private:
		Hmv::string_type old_extension;
		Hmv::string_type new_extension;
	};

	struct FilenameTransform : Transformer
	{
		[[nodiscard]] auto Filter(std::filesystem::path p) const noexcept -> bool
		{
			return p.filename() != old_name;
		}

		[[nodiscard]] auto TransformPath(std::filesystem::path p) const noexcept -> std::filesystem::path
		{
			return p.replace_filename(new_name);
		}
		
		FilenameTransform(Hmv::string_type name_old, Hmv::string_type name_new)
			: old_name(name_old)
			, new_name(name_new)
		{}
	private:
		Hmv::string_type old_name;
		Hmv::string_type new_name;
	};

	struct IncludeFileTransform : Transformer
	{
		[[nodiscard]] auto Filter(std::filesystem::path p) const noexcept -> bool
		{
			return m_exts.contains(p.extension().u8string());
		}

		[[nodiscard]] auto TransformPath(std::filesystem::path p) const noexcept -> std::filesystem::path
		{
			auto const parent = p.parent_path();
			auto const parent_name = parent.filename();
			return parent / m_group_dir / parent_name / p.filename();
		}

		[[nodiscard]] virtual auto TransformInclude(std::filesystem::path old_path, std::filesystem::path new_path) const noexcept -> std::filesystem::path
		{
			return new_path.lexically_relative(old_path.remove_filename() / m_group_dir);
		}

		IncludeFileTransform(std::set<Hmv::string_type> exts, Hmv::string_type group_dir)
			: m_exts(std::move(exts))
			, m_group_dir(group_dir)
		{}
	private:
		std::set<Hmv::string_type> m_exts;
		Hmv::string_type m_group_dir;		
	};

	struct SrcFileTransform : Transformer
	{
		[[nodiscard]] auto Filter(std::filesystem::path p) const noexcept -> bool
		{
			return m_exts.contains(p.extension().u8string());
		}

		[[nodiscard]] auto TransformPath(std::filesystem::path old_path) const noexcept -> std::filesystem::path
		{
			return old_path.parent_path() / m_group_dir / old_path.filename();
		}

		[[nodiscard]] virtual auto TransformInclude(std::filesystem::path old_path, std::filesystem::path new_path) const noexcept -> std::filesystem::path
		{
			return new_path.lexically_relative(old_path.parent_path());
		}

		SrcFileTransform(std::set<Hmv::string_type> exts, Hmv::string_type group_dir)
			: m_exts(std::move(exts))
			, m_group_dir(group_dir)
		{}
	private:
		std::set<Hmv::string_type> m_exts;
		Hmv::string_type m_group_dir;		
	};
}

int main( int argc, char ** argv )
{
	auto const dirs = get_args(argc, argv);

	auto const exts = std::set<Hmv::string_type>{
		u8".h",
		u8".hpp",
		u8".hxx",
		u8".c",
		u8".cpp",
		u8".cxx",
	};

	using header_map = std::unordered_map<Hmv::string_type, std::filesystem::path>;

	std::unique_ptr<Transformer> transformer{ new SrcFileTransform{ { u8".cpp" }, u8"src" } };

	// Preload all header entries
	header_map headers;
	for (iterable recursive_iterator : dirs)
	{
		for (auto const file : recursive_iterator)
		{
			auto const file_path = file.path();
			if (not file.is_regular_file() or not file_path.has_extension()) continue;
			if (not transformer->Filter(file_path)) continue;

			// If this is a header, register it with the headers map.
			headers[file_path.filename().u8string()] = file_path;
		}
	}

	// Load all the files and their headers.
	std::unordered_map<std::filesystem::path, Source> files;
	for (iterable recursive_iterator : dirs)
	{
		for (auto const file : recursive_iterator)
		{
			auto const file_path = file.path();
			if (not file.is_regular_file() or not file_path.has_extension()) continue;
			if (exts.find(file_path.extension().u8string()) == exts.end()) continue;

			// This is a header file, open it.
			auto & header_entry = files[file_path];
			header_entry.old_path = file_path;
			header_entry.new_path = file_path;
			header_entry.contents = open_file(file_path);

			// Find all the headers.
			auto opt_result = Hmv::FindFirstInclude(header_entry.contents);
			while (opt_result.IsValid())
			{
				auto [after, match] = opt_result;
				if (not match.empty())
				{
					auto const c = match.front();
					if (c == '"')
					{
						match.remove_prefix(1);
						match.remove_suffix(1);
						auto const pos = static_cast<sz>(match.data() - header_entry.contents.data());
						if (headers.contains(Hmv::string_type(match)))
						{
							header_entry.q_includes.insert(Include{ .pos = pos, .header = Hmv::string_type(match) });
						}
					}
					else if (c == '<')
					{
						match.remove_prefix(1);
						match.remove_suffix(1);
						auto const pos = static_cast<sz>(match.data() - header_entry.contents.data());
						if (headers.contains(Hmv::string_type(match)))
						{
							header_entry.h_includes.insert(Include{ .pos = pos, .header = Hmv::string_type(match) });
						}
					}
					else
					{
						std::cerr << "Corrupt input stream.\n";
						std::terminate();
					}
				}
				opt_result = Hmv::FindFirstInclude(after);
			}
		}
	}

	// Establish connections between each header, and the references to that header.
	std::unordered_map<Hmv::string_type, Header> header_refs;
	for (auto const & [name, path] : headers)
	{
		header_refs[name].path = path;
	}
	for (auto const & [path, source] : files)
	{
		for (auto const & h_include : source.h_includes)
		{
			header_refs[h_include.header].refs.push_back({
				.path = path,
				.pos = h_include.pos
			});
		}
		for (auto const & q_include : source.q_includes)
		{
			header_refs[q_include.header].refs.push_back({
				.path = path,
				.pos = q_include.pos
			});
		}
	}

	// Building change list.
	for (auto const & [old_name, old_path] : headers)
	{
		auto temp_old_path = old_path;
		auto const new_path = transformer->TransformPath(old_path);
		auto replacement = transformer->TransformInclude(old_path, new_path).u8string();
		std::replace( replacement.begin(), replacement.end(), u8'\\', u8'/');
		if (replacement == old_name) continue;

		auto & header_ref = header_refs[old_name];
		auto & file = files[old_path];
		file.new_path = new_path;
		for (auto const & [path, pos] : header_ref.refs)
		{
			files[path].changes.insert(SourceChange{
				.pos = pos,
				.length = old_name.size(),
				.replacement = replacement,
			});
		}
	}

	// Apply changes
	for (auto & [old_path, file] : files)
	{
		for (auto const & [pos, len, replacement] : file.changes)
		{
			file.contents.replace(pos, len, replacement);
		}
		if (file.old_path != file.new_path)
		{
			move_file(file.old_path, file.new_path);
		}
		if (not file.changes.empty())
		{
			save_file(file.new_path, file.contents);
		}
	}
}