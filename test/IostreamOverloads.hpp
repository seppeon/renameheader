#pragma once
#include "Hmv/FindHeaders.hpp"
#include <ostream>

inline std::ostream & operator<<(std::ostream & o, Hmv::string_view_type const & string)
{
	o << R"(")";
	o.write(reinterpret_cast<const char *>(string.data()), string.size());
	o << R"(")";
	return o;
}

template <typename T>
inline std::ostream & operator<<(std::ostream & o, Hmv::ParseResult<T> const & parse_result)
{
	return o << R"({ "remaining": )" << parse_result.remaining << R"(, "artifact": )" << parse_result.artifact << R"( })";
}