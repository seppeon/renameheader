#include "Hmv/PieceTable.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("PieceTable can append", "[Hmv::PieceTable]")
{
	Hmv::PieceTable table("this is interesting");
	table.Append(" indeed");
	CHECK(table.ToString() == "this is interesting indeed");
}

TEST_CASE("PieceTable can insert", "[Hmv::PieceTable]")
{
	SECTION("Multiple insertions")
	{
		Hmv::PieceTable table("this is interesting");
		table.Insert(4, " indeed");    //  this indeed is interesting
		table.Insert(4, " cat");       //  this cat indeed is interesting
		CHECK(table.ToString() == "this cat indeed is interesting");
	}
	SECTION("Insertion boundary conditions")
	{
		Hmv::PieceTable table("abc");
		SECTION("Start")
		{
			CHECK(table.OriginalContent() == "abc");
			table.Insert(0, "0");
			CHECK(table.OriginalContent() == "abc");
			CHECK(table.ToString() == "0abc");
		}
		SECTION("Middle start")
		{
			table.Insert(1, "1");
			CHECK(table.ToString() == "a1bc");
		}
		SECTION("Middle end")
		{
			table.Insert(2, "2");
			CHECK(table.ToString() == "ab2c");
		}
		SECTION("End")
		{
			table.Insert(3, "3");
			CHECK(table.ToString() == "abc3");
		}
		SECTION("Insert on empty node")
		{
			// Zero everything...
			table.Insert(3, "ABC");
			table.Delete({0, 3});
			table.Insert(0, "123"); 
			CHECK(table.ToString() == "123ABC");
		}
	}
}

TEST_CASE("PieceTable can delete", "[Hmv::PieceTable]")
{
	SECTION("Delete at various potisions of the string")
	{
		Hmv::PieceTable table("four figs goes glib");
		CHECK(table.OriginalContent() == "four figs goes glib");
		table.Delete({4, 5});
		CHECK(table.OriginalContent() == "four figs goes glib");
		CHECK(table.ToString() == "four goes glib");
		table.Delete({0, 5});
		CHECK(table.ToString() == "goes glib");
		table.Delete({5, 4});
		CHECK(table.ToString() == "goes ");
		table.Delete({0, 5});
		CHECK(table.ToString() == "");
	}
	SECTION("Delete over already split sections")
	{
		Hmv::PieceTable table("012345678901234567890123456789");
		//                          |<->|     |<->|
		table.Delete({5, 5});
		CHECK(table.ToString() == "0123401234567890123456789");
		//                                        |<->|
		table.Delete({15, 5});
		CHECK(table.ToString() == "01234012345678956789");
		//                          |<----------->|
		table.Delete({1, 15});
		CHECK(table.ToString() == "06789");
		//                            ||
		table.Delete({3, 2});
		CHECK(table.ToString() == "067");
		//                         ||
		table.Delete({0, 2});
		CHECK(table.ToString() == "7");
		table.Delete({0, 1});
		CHECK(table.ToString() == "");
	}
}

TEST_CASE("PlaceTable can replace", "[Hmv::PieceTable]")
{
	SECTION("Full replace")
	{
		Hmv::PieceTable table("abcdefghi");
		CHECK(table.OriginalContent() == "abcdefghi");
		table.Replace({0, 9}, "012345678");
		CHECK(table.OriginalContent() == "abcdefghi");
		CHECK(table.ToString() == "012345678");
	}
	SECTION("Low")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({0, 3}, "012");
		CHECK(table.ToString() == "012defghi");
	}
	SECTION("Middle")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({3, 3}, "012");
		CHECK(table.ToString() == "abc012ghi");
	}
	SECTION("High")
	{
		Hmv::PieceTable table("abcdefghi");
		//                     012345678
		//                           012
		table.Replace({6, 3}, "012");
		CHECK(table.ToString() == "abcdef012");
	}
	SECTION("Larger")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({6, 3}, "012456");
		CHECK(table.ToString() == "abcdef012456");
	}
	SECTION("Larger starting at beginning")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({0, 3}, "0124567");
		CHECK(table.ToString() == "0124567defghi");
	}
	SECTION("Section fully overlapping a single replaced sections with smaller section")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({3, 3}, "012");
		REQUIRE(table.ToString() == "abc012ghi");
		// In terms of indexes, this is what is expected:
		//                           abc012ghi
		//                           012345678  - Indexes.
		//                              ^^^     - Other node.
		//                             |<->|    - Replaced range.
		//                              \ /   
		//                           01  $  78
		// $: Represents the new string.
		table.Replace({2, 5}, "A");
		CHECK(table.ToString() == "abAhi");
	}
	SECTION("Section left overlapping a single replaced sections with smaller section")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({3, 3}, "012");
		REQUIRE(table.ToString() == "abc012ghi");
		table.Replace({2, 3}, "A");
		CHECK(table.ToString() == "abA2ghi");
	}
	SECTION("Section right overlapping a single replaced sections with smaller section")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({3, 3}, "012");
		REQUIRE(table.ToString() == "abc012ghi");
		table.Replace({4, 3}, "A");
		CHECK(table.ToString() == "abc0Ahi");
	}
	SECTION("Section fully overlapping a single replaced sections with larger section")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({3, 3}, "012");
		REQUIRE(table.ToString() == "abc012ghi");
		table.Replace({2, 5}, "ABCDEF");
		CHECK(table.ToString() == "abABCDEFhi");
	}
	SECTION("Section left overlapping a single replaced sections with larger section")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({3, 3}, "012");
		REQUIRE(table.ToString() == "abc012ghi");
		table.Replace({2, 3}, "ABCDEF");
		CHECK(table.ToString() == "abABCDEF2ghi");
	}
	SECTION("Section right overlapping a single replaced sections with larger section")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({3, 3}, "012");
		REQUIRE(table.ToString() == "abc012ghi");
		table.Replace({4, 3}, "ABCDEF");
		CHECK(table.ToString() == "abc0ABCDEFhi");
	}
	SECTION("Section non-overlapping a single replaced sections with smaller section")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({0, 3}, "012");
		REQUIRE(table.ToString() == "012defghi");
		table.Replace({6, 3}, "ABC");
		CHECK(table.ToString() == "012defABC");
	}
	SECTION("Section non-overlapping a single replaced sections with smaller section")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({1, 3}, "012");
		REQUIRE(table.ToString() == "a012efghi");
		table.Replace({5, 3}, "ABC");
		CHECK(table.ToString() == "a012eABCi");
	}
	SECTION("Can replace a part that extends beyond the end of the existing string")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({1, 3}, "012");
		REQUIRE(table.ToString() == "a012efghi");
		//                           012345678
		//                           |      |
		//                           |      ABC
		//                           a012efgABC
		table.Replace({7, 3}, "ABC");
		CHECK(table.ToString() == "a012efgABC");
	}
	SECTION("Can replace a part that extends beyond the end of the existing string and overlaps parts")
	{
		Hmv::PieceTable table("abcdefghi");
		table.Replace({1, 3}, "012");
		REQUIRE(table.ToString() == "a012efghi");
		//                           012345678
		//                             |
		//                             |
		//                             ABCDEFGH
		//                           a0ABCDEFGH
		table.Replace({2, 10}, "ABCDEFGH");
		CHECK(table.ToString() == "a0ABCDEFGH");
	}
}

namespace 
{
	struct Oracle
	{
		Hmv::string_type content;
		/**
		 * @brief Append a string to the end.
		 * 
		 * @param new_data The data to append to this table.
		 */
		void Append(Hmv::string_view_type new_data)
		{
			content.append(new_data.begin(), new_data.end());
		}
		/**
		 * @brief Replace some section of the piece table.
		 * 
		 * @param old_range The old range (indexes are after applying previous edits).
		 * @param new_data The data that will replace the range of data specified by `old_range`.
		 */
		void Replace(Hmv::PieceTableNode old_range, Hmv::string_view_type new_data)
		{
			content.replace(content.begin() + old_range.start, content.begin() + old_range.start + old_range.length, new_data);
		}
		/**
		 * @brief Delete a section of the piece table.
		 * 
		 * @param range The range in the piece table (after applying previous edits) that will be deleted.
		 */
		void Delete(Hmv::PieceTableNode range)
		{
			content.erase(content.begin() + range.start, content.begin() + range.start + range.length);
		}
		/**
		 * @brief Apply an insertion to the piece table.
		 * 
		 * @param index The index in the piece table (after applying previous edits) that the data will be inserted.
		 * @param new_data The new data to insert.
		 */
		void Insert(std::size_t index, Hmv::string_view_type new_data)
		{
			content.insert(index, new_data.data(), new_data.size());
		}
	};
}

TEST_CASE("Random testing")
{
    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::size_t> length_distribution(0,100);
    std::uniform_int_distribution<std::uint32_t> letter_distribution(std::uint32_t('a'),std::uint32_t('z'));

	auto const random_string_length = [&]
	{
		return length_distribution(rng);
	};
	auto const random_letter = [&]
	{
		return (char)letter_distribution(rng);
	};
	auto const random_string = [&]
	{
		auto output = std::string(random_string_length(), '\0');
		for (auto & c : output)
		{
			c = random_letter();
		}
		return output;
	};
	auto const random_index = [&](std::size_t max_index)
	{
    	std::uniform_int_distribution<std::mt19937::result_type> dist(0, max_index);
		return dist(rng);
	};
	auto const random_length = [&](std::size_t max_length)
	{
    	std::uniform_int_distribution<std::mt19937::result_type> dist(0, max_length);
		return dist(rng);
	};

	constexpr auto fuzz_count = std::size_t(1000);

	SECTION("Append")
	{
		for (std::size_t i = 0; i < fuzz_count; ++i)
		{
			auto const initial_string = random_string();
			auto a = Hmv::PieceTable(initial_string);
			auto b = Oracle(initial_string);
			for (std::size_t x = 0; x < random_length(100); ++x)
			{
				auto const string = random_string();
				a.Append(string);
				b.Append(string);
			}
			CHECK(a.ToString() == b.content);
			Hmv::string_type data;
			for (std::size_t i = 0; i < a.Size(); ++i)
			{
				data += a[i];
			}
			CHECK(b.content == data);
		}
	}

	SECTION("Delete")
	{
		for (std::size_t i = 0; i < fuzz_count; ++i)
		{
			auto const initial_string = random_string();
			auto a = Hmv::PieceTable(initial_string);
			auto b = Oracle(initial_string);
			for (std::size_t x = 0; x < random_length(100); ++x)
			{
				auto const index = random_index(b.content.size());
				auto const length = random_length(b.content.size() - index);
				a.Delete({index, length});
				b.Delete({index, length});
				CHECK(a.ToString() == b.content);
				Hmv::string_type data;
				for (std::size_t i = 0; i < a.Size(); ++i)
				{
					data += a[i];
				}
				CHECK(b.content == data);
			}
		}
	}

	SECTION("Insert")
	{
		for (std::size_t i = 0; i < fuzz_count; ++i)
		{
			auto const initial_string = random_string();
			auto a = Hmv::PieceTable(initial_string);
			auto b = Oracle(initial_string);
			for (std::size_t x = 0; x < random_length(200); ++x)
			{
				auto const index = random_index(b.content.size());
				auto const string = random_string();
				a.Insert(index, string);
				b.Insert(index, string);
				CHECK(a.ToString() == b.content);
				Hmv::string_type data;
				for (std::size_t i = 0; i < a.Size(); ++i)
				{
					data += a[i];
				}
				CHECK(b.content == data);
			}
		}
	}

	SECTION("Replace")
	{
		for (std::size_t i = 0; i < fuzz_count; ++i)
		{
			auto const initial_string = random_string();
			auto a = Hmv::PieceTable(initial_string);
			auto b = Oracle(initial_string);
			for (std::size_t x = 0; x < random_length(100); ++x)
			{
				auto const index = random_index(b.content.size());
				auto const length = random_length(b.content.size() - index);
				auto const string = random_string();
				a.Replace({ index, length }, string);
				b.Replace({ index, length }, string);
				CHECK(a.ToString() == b.content);
				Hmv::string_type data;
				for (std::size_t i = 0; i < a.Size(); ++i)
				{
					data += a[i];
				}
				CHECK(b.content == data);
			}
		}
	}
}