#include "Hmv/FenwickTree.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("Fenwick tree functions", "[Hmv::FenwickTree]")
{
	auto tree = Hmv::FenwickTree<std::size_t>{2, 4, 8, 16, 32, 64, 128, 256};
	CHECK(tree.QueryLength(0) == 2);
	CHECK(tree.QueryLength(1) == 4);
	CHECK(tree.QueryLength(2) == 8);
	CHECK(tree.QueryLength(3) == 16);
	CHECK(tree.QueryLength(4) == 32);
	CHECK(tree.QueryLength(5) == 64);
	CHECK(tree.QueryLength(6) == 128);
	CHECK(tree.QueryLength(7) == 256);
	CHECK(tree.QuerySum(8) == 510);
	CHECK(tree.PushBack(512) == 8);
	CHECK(tree.PushBack(1024) == 9);
	CHECK(tree.PushBack(2048) == 10);
	CHECK(tree.QuerySum(9) == 1022);
	CHECK(tree.QuerySum(10) == 2046);
	CHECK(tree.QuerySum(11) == 4094);
	tree.Update(0, 10);
	CHECK(tree.QueryLength(0) == 12);
	CHECK(tree.QueryLength(1) == 4);
	CHECK(tree.QueryLength(2) == 8);
	CHECK(tree.QueryLength(3) == 16);
	CHECK(tree.QueryLength(4) == 32);
	CHECK(tree.QueryLength(5) == 64);
	CHECK(tree.QueryLength(6) == 128);
	CHECK(tree.QueryLength(7) == 256);
	CHECK(tree.QueryLength(8) == 512);
	CHECK(tree.QueryLength(9) == 1024);
	CHECK(tree.QueryLength(10) == 2048);

	SECTION("One size tree")
	{
		auto tree_1 = Hmv::FenwickTree<std::size_t>{2};
		CHECK(tree_1.QuerySum(0) == 0);
		tree_1.Update(1, 10);

		auto tree_2 = Hmv::FenwickTree<std::size_t>{2};
		CHECK(tree_1.QuerySum(0) == tree_2.QuerySum(0));
	}
	SECTION("Three size tree")
	{
		auto tree_1 = Hmv::FenwickTree<std::size_t>{2, 4, 1};
		CHECK(tree_1.QuerySum(0) == 0);
		CHECK(tree_1.QuerySum(1) == 2);
		CHECK(tree_1.QuerySum(2) == 2+4);
		CHECK(tree_1.QuerySum(3) == 2+4+1);
		tree_1.SetLength(1, 10);

		auto tree_2 = Hmv::FenwickTree<std::size_t>{2, 10, 1};
		CHECK(tree_1.QuerySum(0) == tree_2.QuerySum(0));
		CHECK(tree_1.QuerySum(1) == tree_2.QuerySum(1));
		CHECK(tree_1.QuerySum(2) == tree_2.QuerySum(2));
		CHECK(tree_1.QuerySum(3) == tree_2.QuerySum(3));
	}
	SECTION("Pow 2 tree")
	{
		auto tree_1 = Hmv::FenwickTree<std::size_t>{2, 4, 1, 7};
		CHECK(tree_1.QuerySum(0) == 0);
		CHECK(tree_1.QuerySum(1) == 2);
		CHECK(tree_1.QuerySum(2) == 2+4);
		CHECK(tree_1.QuerySum(3) == 2+4+1);
		CHECK(tree_1.QuerySum(4) == 2+4+1+7);
		tree_1.Update(1, 10);

		auto tree_2 = Hmv::FenwickTree<std::size_t>{2, 4+10, 1, 7};
		CHECK(tree_1.QuerySum(0) == tree_2.QuerySum(0));
		CHECK(tree_1.QuerySum(1) == tree_2.QuerySum(1));
		CHECK(tree_1.QuerySum(2) == tree_2.QuerySum(2));
		CHECK(tree_1.QuerySum(3) == tree_2.QuerySum(3));
		CHECK(tree_1.QuerySum(4) == tree_2.QuerySum(4));
	}
	SECTION("Uneven tree")
	{
		auto tree_1 = Hmv::FenwickTree<std::size_t>{2, 4, 1, 7, 4};
		CHECK(tree_1.QuerySum(0) == 0);
		CHECK(tree_1.QuerySum(1) == 2);
		CHECK(tree_1.QuerySum(2) == 2+4);
		CHECK(tree_1.QuerySum(3) == 2+4+1);
		CHECK(tree_1.QuerySum(4) == 2+4+1+7);
		CHECK(tree_1.QuerySum(5) == 2+4+1+7+4);
		tree_1.Update(1, 10);

		auto tree_2 = Hmv::FenwickTree<std::size_t>{2, 4+10, 1, 7, 4};
		CHECK(tree_1.QuerySum(0) == tree_2.QuerySum(0));
		CHECK(tree_1.QuerySum(1) == tree_2.QuerySum(1));
		CHECK(tree_1.QuerySum(2) == tree_2.QuerySum(2));
		CHECK(tree_1.QuerySum(3) == tree_2.QuerySum(3));
		CHECK(tree_1.QuerySum(4) == tree_2.QuerySum(4));
		CHECK(tree_1.QuerySum(5) == tree_2.QuerySum(5));
	}
}