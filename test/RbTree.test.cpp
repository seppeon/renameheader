#include "Hmv/RbTree.hpp"
#include <ostream>
#include <ranges>
#include <map>
#include <fstream>
#include <catch2/catch_all.hpp>


TEST_CASE("A red black tree can insert", "[Hmv::RbTree]")
{
	using rb_tree = Hmv::RbTree<int, char>;
	rb_tree tree;
	for (int v = 0; v < 26; ++v)
	{
		CHECK(tree.Insert(v, char('a' + v)));
	}
	auto const get_value = [](rb_tree::iterator::reference const & v)
	{
		return v.value;
	};
	auto const parts_transform = tree | std::ranges::views::transform(get_value);
	typename decltype(parts_transform.begin())::iterator_concept ads;
	std::string parts_string( parts_transform.begin(), parts_transform.end() );
	CHECK(parts_string == "abcdefghijklmnopqrstuvwxyz");
	CHECK(tree.MaxDepth() == 6);
} 

TEST_CASE("A red black tree can remove", "[Hmv::RbTree]")
{
	Hmv::RbTree<int, int> tree;
	CHECK(tree.Insert(12, 12));
	CHECK(tree.Insert(8, 8));
	CHECK(tree.Insert(15, 15));
	CHECK(tree.Insert(1, 1));
	CHECK(tree.Insert(23, 23));
	CHECK(tree.Insert(10, 10));
	CHECK(tree.Insert(9, 9));
	CHECK(tree.Insert(13, 13));

	std::ofstream out("F:/Files/Git/rename_header/include/Hmv/scratch.md", std::ofstream::trunc);
	out << "# From\n";
	out << "```mermaid\n";
	out << tree.ToString() << "\n";
	out << "```" << std::endl;
	auto test_removal = [&tree, &out](int node) mutable
	{
		auto copy = tree;
		CHECK(copy.Remove(node));
		out << "# Removed " << node << " \n";
		out << "```mermaid\n";
		out << copy.ToString() << "\n";
		out << "```" << std::endl;
	};

	test_removal(12);
	test_removal(1);
	test_removal(10);
	test_removal(8);
	test_removal(9);
	test_removal(15);
	test_removal(13);
	test_removal(23);
}

// TEST_CASE("RbTree is faster than std::map")
// {
// 	BENCHMARK("Insert 1000000 items, Hmv::RbTree<int, char>")
// 	{
// 		Hmv::RbTree<int, int> tree;
// 		for (int v = 0; v < 1000000; ++v)
// 		{
// 			(void)tree.Insert(v, int(v));
// 		}
// 		return tree;
// 	};
// 	BENCHMARK("Insert 1000000 items, std::map<int, int>")
// 	{
// 		std::map<int, int> tree;
// 		for (int v = 0; v < 1000000; ++v)
// 		{
// 			tree.insert({v, int(v)});
// 		}
// 		return tree;
// 	};
// 	BENCHMARK("Insert 1000000 items, std::unordered_map<int, int>")
// 	{
// 		std::unordered_map<int, int> tree;
// 		for (int v = 0; v < 1000000; ++v)
// 		{
// 			tree.insert({v, int(v)});
// 		}
// 		return tree;
// 	};
// }