#include "Hmv/FindHeaders.hpp"
#include "IostreamOverloads.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("Header strings can be parsed", "[Hmv::FindFirstInclude]")
{
	auto const quoted_angled = Hmv::HeaderSearchOptions{ .include_quoted = true, .include_angle_brackets = true };
	auto const check = [&](Hmv::string_view_type test, Hmv::string_view_type remaining, Hmv::string_view_type artifact)
	{
		auto const result = Hmv::ParseIncludeString(test, quoted_angled);
		auto const expected = Hmv::ParseIncludeResult{ .remaining = remaining, .artifact = artifact };
		CHECK(result == expected);
	};
	auto const check_invalid = [&](Hmv::string_view_type test)
	{
		auto const result = Hmv::ParseIncludeString(test, quoted_angled);
		CHECK(not result.IsValid());
	};
	check(R"("hello")",      "",      R"("hello")" );
	check(R"("hello"abc)",   "abc",   R"("hello")" );
	check(R"(<hello>)",      "",      R"(<hello>)" );
	check(R"(<hello>abc)",   "abc",   R"(<hello>)" );
	check_invalid(R"(  "hello")");
	check_invalid(R"(  "hello"abc)");
	check_invalid(R"(  <hello>)");
	check_invalid(R"(  <hello>abc)");

	// Check at various parts of the parse
	check_invalid(R"("hello)"); // mising close quote.
	check_invalid(R"(")"); // mising header name and closing quote.
	check_invalid(R"()"); // mising everything.
}

TEST_CASE("Include strings can be parsed", "[Hmv::FindFirstInclude]")
{
	auto const quoted_angled = Hmv::HeaderSearchOptions{ .include_quoted = true, .include_angle_brackets = true };
	auto const check = [&](Hmv::string_view_type test, Hmv::string_view_type remaining, Hmv::string_view_type artifact)
	{
		auto const result = Hmv::ParseInclude(test, quoted_angled);
		auto const expected = Hmv::ParseIncludeResult{ .remaining = remaining, .artifact = artifact };
		CHECK(result == expected);
	};
	auto const check_invalid = [&](Hmv::string_view_type test)
	{
		auto const result = Hmv::ParseInclude(test, quoted_angled);
		CHECK(not result.IsValid());
	};
	check(R"(#include"hello")",         "",      R"("hello")" );
	check(R"(#include"hello"abc)",      "abc",   R"("hello")" );
	check(R"(#include<hello>)",         "",      R"(<hello>)" );
	check(R"(#include<hello>abc)",      "abc",   R"(<hello>)" );
	check(R"(  #include"hello")",       "",      "\"hello\"" );
	check(R"(  #include"hello"abc)",    "abc",   "\"hello\"" );
	check(R"(  #include<hello>)",       "",      "<hello>" );
	check(R"(  #include<hello>abc)",    "abc",   "<hello>" );
	check(R"(#include   "hello")",      "",      R"("hello")" );
	check(R"(#include   "hello"abc)",   "abc",   R"("hello")" );
	check(R"(#include   <hello>)",      "",      R"(<hello>)" );
	check(R"(#include   <hello>abc)",   "abc",   R"(<hello>)" );
	check(R"(  #include   "hello")",    "",      "\"hello\"" );
	check(R"(  #include   "hello"abc)", "abc",   "\"hello\"" );
	check(R"(  #include   <hello>)",    "",      "<hello>" );
	check(R"(  #include   <hello>abc)", "abc",   "<hello>" );
	check_invalid(R"(a  #include   "hello")");
	check_invalid(R"(  #include  a "hello"abc)");
	check_invalid(R"(  a#includea   <hello>)");
	check_invalid(R"(  #includea   a<hello>abc)");

	// Check partial parses.	
	check_invalid(R"(#include  "hello)"); // Missing close quote.
	check_invalid(R"(#include  ")"); // Missing include text.
	check_invalid(R"(#include  )"); // Missing open quote.
	check_invalid(R"(#include)"); // Missing open space.
	check_invalid(R"()"); // Missing everything.
}

TEST_CASE("The first include can be found", "[Hmv::FindFirstInclude]")
{
	auto const quoted_angled = Hmv::HeaderSearchOptions{ .include_quoted = true, .include_angle_brackets = true };
	auto const check = [&](Hmv::string_view_type test, Hmv::string_view_type remaining, Hmv::string_view_type artifact)
	{
		auto const result = Hmv::FindFirstInclude(test, quoted_angled);
		auto const expected = Hmv::ParseIncludeResult{ .remaining = remaining, .artifact = artifact };
		CHECK(result == expected);
	};
	auto const check_invalid = [&](Hmv::string_view_type test)
	{
		auto const result = Hmv::FindFirstInclude(test, quoted_angled);
		CHECK(not result.IsValid());
	};

	check("#include<vector>this is more\ntext\n", "this is more\ntext\n", "<vector>");
	check("this is a prefix\n#include<vector>this is more\ntext\n", "this is more\ntext\n", "<vector>");
	check("(this is a prefix\nthis is more\ntext\n#include <string>", "", "<string>");

	check_invalid("#include<vectorthis is more\ntext\n"); // missing close angle.
	check_invalid("#include< this is more\ntext\n"); // missing header name.
	check_invalid("#includethis is more\ntext\n"); // missing open angle.
	check_invalid("this is more\ntext\n"); // missing everything
	check_invalid("this is a prefix\n#include<vectorthis is more\ntext\n"); // missing close angle.
	check_invalid("this is a prefix\n#include< this is more\ntext\n"); // missing header name.
	check_invalid("this is a prefix\n#includethis is more\ntext\n"); // missing open angle.
	check_invalid("this is a prefix\nthis is more\ntext\n"); // missing everything
}

static constexpr Hmv::string_view_type string_with_many_headers =
R"(
#include <algorithm>
#include <filesystem>
#include <iostream>
#include <memory>
#include <optional>
#include <ranges>
#include <fstream>
#include <set>
#include <span>
#include <streambuf>
#include <string>
#include <string_view>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "Hmv/Hmv.hpp"
#include "hmv/FindHeaders.hpp"
// #include <FL/File.hpp>

namespace
{
	Hmv::ParseResult<int> temp;

	using sz = std::size_t;
	using ssz = std::make_signed_t<sz>;

	auto get_args( int argc, char ** argv ) -> std::vector<std::filesystem::path>
	{
		std::vector<std::filesystem::path> output;
		for ( int i = 1; i < argc; ++i ) output.push_back( { argv[i] } );
)";
static constexpr Hmv::string_view_type string_with_zero_headers = 
R"(
// #include <algorithm>
// #include <filesystem>
// #include <iostream>
// #include <memory>
// #include <optional>
// #include <ranges>
// #include <fstream>
// #include <set>
// #include <span>
// #include <streambuf>
// #include <string>
// #include <string_view>
// #include <tuple>
// #include <unordered_map>
// #include <unordered_set>
// #include <vector>
// #include "Hmv/Hmv.hpp"
// #include "hmv/FindHeaders.hpp"
// #include <FL/File.hpp>

namespace
{
	Hmv::ParseResult<int> temp;

	using sz = std::size_t;
	using ssz = std::make_signed_t<sz>;

	auto get_args( int argc, char ** argv ) -> std::vector<std::filesystem::path>
	{
		std::vector<std::filesystem::path> output;
		for ( int i = 1; i < argc; ++i ) output.push_back( { argv[i] } );
)";
static constexpr Hmv::string_view_type headers_with_block_comments = 
R"(
#include <algorithm>
#include <filesystem>
#include <iostream>
#include <memory>
/*
#include <optional>
#include <ranges>
#include <fstream>
#include <set>
#include <span>
#include <streambuf>
#include <string>
inline constexpr char buffer[] = "*/";
#include <string_view>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "Hmv/Hmv.hpp"
#include "hmv/FindHeaders.hpp"
#include <FL/File.hpp>
*/

namespace
{
	Hmv::ParseResult<int> temp;

	using sz = std::size_t;
	using ssz = std::make_signed_t<sz>;

	auto get_args( int argc, char ** argv ) -> std::vector<std::filesystem::path>
	{
		std::vector<std::filesystem::path> output;
		for ( int i = 1; i < argc; ++i ) output.push_back( { argv[i] } );
)";

TEST_CASE("All includes can be found", "[Hmv::FindAllIncludes]")
{
	auto const many_includes = Hmv::FindAllIncludes(string_with_many_headers);
	auto const many_includes_expected_result = Hmv::HeaderList{
		"<algorithm>",
		"<filesystem>",
		"<iostream>",
		"<memory>",
		"<optional>",
		"<ranges>",
		"<fstream>",
		"<set>",
		"<span>",
		"<streambuf>",
		"<string>",
		"<string_view>",
		"<tuple>",
		"<unordered_map>",
		"<unordered_set>",
		"<vector>",
		"\"Hmv/Hmv.hpp\"",
		"\"hmv/FindHeaders.hpp\"",
	};
	CHECK(many_includes_expected_result == many_includes);

	auto const zero_includes = Hmv::FindAllIncludes(string_with_zero_headers);
	auto const zero_includes_expected_result = Hmv::HeaderList{};
	CHECK(zero_includes_expected_result == zero_includes);

	auto const headers_with_comments = Hmv::FindAllIncludes(headers_with_block_comments);
	auto const headers_with_comments_expected_result = Hmv::HeaderList{
		"<algorithm>",
		"<filesystem>",
		"<iostream>",
		"<memory>",
	};
	CHECK(headers_with_comments_expected_result == headers_with_comments);
}