A tool to search a directory for source files, detect used headers then rename the header file itself and the references to that header file. This is useful when changing extensions of headers.

If you wanted to seach these three folders:
 - c:\temp\docs
 - c:\bin
 - d:\cats

Then you could execute the application as follows:
```
RenameHeader.exe c:\temp\docs c:\bin d:\cats
```

At the moment the repository is in flex during a large upgrade. The upgrade is to ensure correct behaviour in this case:

```CPP
#include <algorithm>
/*
#include <optional>
inline constexpr char buffer[] = "*/";
#include <string_view>
*/
```
It is likely the git platform ahs rendered the above incorrectly, the included headers should be only `<algorithm>`, but without correct preprocessing of strings, it is not possible to process comment blocks correctly. There are similar issues with preprocessor `#ifdef` blocks, however in these cases renames are usually desirable.

The upgrade will also be adding support for correct include resolution as per the order defined in some `compile_commands.json`. Without such support, the tool is usable, but can at times perform renames that are not as expected.